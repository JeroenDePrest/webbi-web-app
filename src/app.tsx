import { AdminLayoutContainer } from '@components/admin/adminLayout/adminLayoutContainer';
import { AppLayoutContainer, NavItem } from '@components/app/appLayout/appLayoutContainer';
import { PrivateRoute } from '@components/common/privateRoute/privateRoute';
import { StripeHookProvider } from '@components/stripe/stripe';
import { AccountPickerContainer } from '@pages/accountPicker/accountPickerContainer';
import { BillingPanelContainer } from '@pages/admin/billingPanel/billingPanelContainer';
import { WebsiteDetailContainer } from '@pages/admin/websiteDetail/websiteDetailContainer';
import { WebsitesPanelContainer } from '@pages/admin/websitesPanel/websitesPanelContainer';
import { LoginSuccessContainer } from '@pages/auth/loginSuccess/loginSuccessContainer';
import { ConfiguratorContainer } from '@pages/configurator/configurator/configuratorContainer';
import { LogoutContainer } from '@pages/logout/logoutContainer';
import { MainContainer } from '@pages/main/mainContainer';
import { PublicPageContainer } from '@pages/publicPage/publicPageContainer';
import { SuccessContainer } from '@pages/success/successContainer';
import { TemplatePickerContainer } from '@pages/templatePicker/templatePickerContainer';
import { AuthState } from '@store/reducers/authReducer';
import { getSubdomain } from '@utils/domain';
import { store } from '@utils/habitat/HabitatApp';
import { Dropdown, Icon, Menu } from 'antd';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import messages from './translations/nl.json';
import './utils/habitat/HabitatApp';

const renderNav = (isAuthenticated: boolean, user: AuthState['user']): NavItem[] => {
	if (isAuthenticated) {
		return [
			{
				name: user.name,
				component: (
					<Dropdown
						trigger={['click']}
						placement="bottomCenter"
						overlay={
							<Menu>
								{/* <Menu.Item>
									<Link to="/admin/billing">Billing</Link>
								</Menu.Item> */}
								<Menu.Item>
									<Link to="/logout">Logout</Link>
								</Menu.Item>
							</Menu>
						}>
						<div>
							{user.name}
							<Icon type="down" />
						</div>
					</Dropdown>
				),
				shouldRender: isAuthenticated,
				to: '',
				exact: true,
				icon: <Icon type="home" />,
			},
		];
	}

	return [];
};

export const App: React.StatelessComponent = () => (
	<Provider store={store}>
		<IntlProvider messages={messages} locale="nl-BE">
			<BrowserRouter>
				<StripeHookProvider apiKey="pk_test_tt44nyQVK1RTUYVZswEnugMj00Crfda8lI">
					<Switch>
						<Route path="/login/succes" component={LoginSuccessContainer} />
						<Route exact path="/logout" component={LogoutContainer} />


						<PrivateRoute
							exact
							path="/configurator/:instagramId"
							component={ConfiguratorContainer}
						/>

						<Route exact path="/user/:username" component={PublicPageContainer} />
						<Route
							path="/admin/*"
							exact
							render={() => (
								<AdminLayoutContainer
									header
									// sidenav
									// renderSideBarItems={renderSideBarNav}
									contained
									renderItems={renderNav}>
									<PrivateRoute
										exact
										path="/admin/websites/:username"
										component={WebsiteDetailContainer}
									/>
									<PrivateRoute
										exact
										path="/admin/websites"
										component={WebsitesPanelContainer}
									/>
									<PrivateRoute
										exact
										path="/admin/billing"
										component={BillingPanelContainer}
									/>
								</AdminLayoutContainer>
							)}
						/>
						<Route
							path="*"
							render={() => {
								const subdomain = getSubdomain();
								const { 1: root } = window.location.hostname.split(`${subdomain}.`);

								if (
									root === 'webbi.co' &&
									subdomain &&
									subdomain.length &&
									subdomain !== 'www' &&
									subdomain.indexOf('.') === -1
								) {
									return <PublicPageContainer username={subdomain} />;
								}

								return (
									<AppLayoutContainer renderItems={renderNav}>
										<PrivateRoute
											exact
											path="/templates"
											component={TemplatePickerContainer}
										/>

										<PrivateRoute
											exact
											path="/success"
											component={SuccessContainer}
										/>

										<PrivateRoute
											exact
											path="/accounts"
											component={AccountPickerContainer}
										/>
										<Route exact path="/" component={MainContainer} />
										{/* <PrivateRoute exact path="/app/preview" component={PreviewContainer} /> */}
										{/* <PrivateRoute exact path="/app/checkout" component={CheckoutContainer} /> */}
									</AppLayoutContainer>
								);
							}}
						/>
						{/* <Route
						path="*"
						render={() => (
							<MainLayoutContainer renderItems={renderNav}>
								<Route exact path="/" component={MainContainer} />
								
							</MainLayoutContainer>
						)}
					/> */}
					</Switch>
				</StripeHookProvider>
			</BrowserRouter>
		</IntlProvider>
	</Provider>
);
