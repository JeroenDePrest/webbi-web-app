import { autobind } from 'core-decorators';
import * as React from 'react';

import { AccountCardProps } from '@components/accountCard/accountCardContainer';
import {
	AccountCardStyle,
	AccountDescription,
	AccountName,
	Avatar,
	ImageCropper,
} from '@components/accountCard/accountCardStyle';

interface State {}

@autobind
export class AccountCard extends React.Component<AccountCardProps, State> {
	public render() {
		const { account } = this.props;

		return (
			<AccountCardStyle onClick={this.selectAccount}>
				<ImageCropper>
					<Avatar src={account.profilePictureUrl} />
				</ImageCropper>
				<AccountName>{account.pageName}</AccountName>
				<AccountDescription>@{account.username}</AccountDescription>
			</AccountCardStyle>
		);
	}

	private selectAccount() {
		const { account, websites } = this.props;

		this.props.selectAccount(account);

		const selectedExisting = websites.includes(account.username);

		if (selectedExisting) {
			this.props.setEdit(selectedExisting);
		} else {
			this.props.selectAccount(account);
		}

		this.props.history.push('/templates');
	}
}
