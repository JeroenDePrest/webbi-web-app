import { AccountCard } from '@components/accountCard/accountCard';
import { selectAccount } from '@store/actions/configurator/selectAccount';
import { setEdit } from '@store/actions/configurator/setEdit';
import { StoreState } from '@store/reducers/root';
import { History } from 'history';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { compose, Dispatch } from 'redux';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';

export interface AccountCardContainerProps {
	account: InstagramBusinessAccount;
	history: History;
}

export const mapStateToProps = (state: StoreState) => {
	return {
		inConfigurationProcess: state.configurator.inConfigurationProcess,
		websites: state.general.websites,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		selectAccount: (account: InstagramBusinessAccount) => dispatch(selectAccount({ account })),
		setEdit: (edit: boolean) => dispatch(setEdit({ inConfigurationProcess: edit })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const AccountCardContainer = compose<React.ComponentType<AccountCardContainerProps>>(
	withRedux
)(AccountCard);

export type AccountCardProps = AccountCardContainerProps &
	RouteComponentProps &
	StateProps &
	DispatchProps;
