import styled from 'styled-components';

export const AccountCardStyle = styled.div`
    width: 200px;
    height: fit-content;
    
    border-radius: 6px;
    
    color: rgba(0,0,0,.85);
    
    background-color: #FFF;
    
    margin: 16px 12px;
    box-shadow: 0px 10px 30px rgba(0,0,0,.08);
    
    display: flex;
    align-items: center;
    flex-direction: column;
    
    padding: 24px;
    
    :hover{
        cursor: pointer;
    }
`;

export const Avatar = styled.img`
    display: inline;
    margin: 0 auto;
    /* margin-left: -25%;  */
    height: 100%;
    width: auto;
    /* box-shadow: inset 0 2px 4px 0 hsla(0, 0%, 0%, .2); */
`;

export const AccountName = styled.div`
    font-weight: 800;
    font-size: 20px;
    margin-top: 10px;
`;

export const AccountDescription = styled.div`
    /* margin-top: 10px; */
    color: rgba(0,0,0,0.4);
    text-align:center;
`;

export const ImageCropper = styled.div`
    width: 75px;
    height: 75px;
    position: relative;
    overflow: hidden;
    border-radius: 50%;
    /* box-shadow: inset 0 5px 10px 0 hsla(0, 0%, 0%, 1); */
`;

export const Nav = styled.div`
    padding: 16px;
    font-size: 20px;
    i{
        margin-right: 10px;
    }
`;
