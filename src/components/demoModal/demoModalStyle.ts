import styled, { createGlobalStyle } from 'styled-components';

export const DemoModalStyle = styled.div``;
export const ModalHeader = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;

	padding: 10px 20px;
`;

export const TemplateInfo = styled.div`
	display: flex;
	align-items: center;
`;

export const TemplateTitle = styled.h3`
	margin-bottom: 0;
	margin-right: 40px;
	text-transform: capitalize;
`;

export const CloseIcon = styled.div`
	color: rgba(0, 0, 0, 0.85);
	font-size: 20px;
`;

export const ModalStyle = createGlobalStyle`
    div.modalWrapper iframe#demo {
        width: 100%;
        height: 100%;
    }

    div.modalWrapper div.ant-modal {
        height: 100%;
        width: 100% !important;
        top:0 !important;
        padding-bottom: 0 !important;
    }

    div.modalWrapper div.ant-modal-content {
        height: 100%;
        border-radius:0 !important;
    }

    div.modalWrapper div.ant-modal-body {
        height: 100%;
        /* overflow:auto; */
        padding:0 !important;
    }
    div.modalWrapper div.ant-modal-footer {
        display:none;
    }
    div.modalWrapper button.ant-modal-close {
        display:none;
    }
`;
