import { Template } from '@api/templates';
import {
	CloseIcon,
	DemoModalStyle,
	ModalHeader,
	ModalStyle,
	TemplateInfo,
	TemplateTitle,
} from '@components/demoModal/demoModalStyle';
import { Button, Icon, Modal, Spin } from 'antd';
import * as React from 'react';

export interface DemoModalProps {
	visible: boolean;
	url: string;
	selectedTemplate?: Template;
	innerHtml: string | undefined;
	handleOk(): void;
	handleCancel(): void;
}

export const DemoModal: React.StatelessComponent<DemoModalProps> = (props: DemoModalProps) => {
	return (
		<DemoModalStyle>
			<ModalStyle />
			<Modal
				visible={props.visible}
				onOk={props.handleOk}
				onCancel={props.handleCancel}
				wrapClassName="modalWrapper"
				footer={
					[
						// <Button key="back" onClick={props.handleCancel}>
						//     Return
						// 	</Button>,
						// <Button key="submit" type="primary" onClick={props.handleOk}>
						//     Use template
						// 	</Button>,
					]
				}
				style={{ top: 20 }}>
				<ModalHeader>
					<TemplateInfo>
						<TemplateTitle>
							{props.selectedTemplate &&
								props.selectedTemplate.name.replace(/_/g, ' ')}
						</TemplateTitle>
						<div>
							<Button key="submit" type="primary" onClick={props.handleOk}>
								Use template
							</Button>
						</div>
					</TemplateInfo>
					<CloseIcon>
						<Icon onClick={props.handleCancel} type="close" />
					</CloseIcon>
				</ModalHeader>
				{props.innerHtml ? (
					<div id="htmlArea" dangerouslySetInnerHTML={{ __html: props.innerHtml }}>
						{/* <iframe src={props.url} id="demo" title="demo" sandbox="allow-scripts" /> */}
					</div>
				) : (
					<Spin />
				)}
			</Modal>
		</DemoModalStyle>
	);
};
