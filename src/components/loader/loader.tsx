import * as React from 'react';
import { LoaderStyle } from '@components/loader/loaderStyle';
import { Icon, Spin } from 'antd';

export interface LoaderProps {}

export const Loader: React.StatelessComponent<LoaderProps> = (props: LoaderProps) => {
	return (
		<LoaderStyle>
			<Spin indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />} />
		</LoaderStyle>
	);
};
