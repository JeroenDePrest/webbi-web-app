import { compose, Dispatch } from 'redux';

import { AppLayout } from '@components/app/appLayout/appLayout';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { AuthState } from '@store/reducers/authReducer';
import { ReactNode } from 'react';

export interface AppLayoutContainerProps {
	dark?: boolean;
	sidenav?: boolean;
	header?: boolean;
	contained?: boolean;
	renderItems?(isAuthenticated: boolean, user?: AuthState['user']): NavItem[];
}

export interface NavItem {
	to: string;
	shouldRender: boolean;
	name: string | ReactNode;
	icon?: ReactNode;
	exact?: boolean;
	component?: ReactNode;
}

export const mapStateToProps = (state: StoreState) => {
	return {
		isAuthenticated: state.auth.isAuthenticated,
		user: state.auth.user,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const AppLayoutContainer = compose<React.ComponentType<AppLayoutContainerProps>>(withRedux)(
	AppLayout
);

export type AppLayoutProps = AppLayoutContainerProps & StateProps & DispatchProps;
