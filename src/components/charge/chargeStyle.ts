// tslint:disable:import-name
import { Col, Row } from 'antd';
import styled from 'styled-components';

export const ChargesStyle = styled.div`
	padding: 10px 0px;
	border-bottom: 1px solid rgb(226, 232, 240);
`;

export const ChargeRecordWrapper = styled(Row)`
	display: flex;
	justify-content: space-between;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
`;

export const RowItem = styled(Col)`
	@media (max-width: 767px) {
		text-align: center;
	}
`;

export const RowItemRight = styled(RowItem)`
	text-align: right;
`;
