/* eslint @typescript-eslint/camelcase: 0 */

import { ChargeItem } from '@api/charge';
import {
	ChargeRecordWrapper,
	ChargesStyle,
	RowItem,
	RowItemRight,
} from '@components/charge/chargeStyle';
import moment from 'moment';
import * as React from 'react';

export interface ChargeProps {
	charge: ChargeItem;
}

export const Charge: React.StatelessComponent<ChargeProps> = (props: ChargeProps) => {
	const { amount, created, currency, payment_method_details, receipt_url, id } = props.charge;

	return (
		<ChargesStyle>
			<ChargeRecordWrapper>
				<RowItem xs={24} sm={24} md={{ span: 3, offset: 0 }} lg={{ span: 3 }}>
					{moment(created * 1000).format('ll')}
				</RowItem>
				<RowItem md={{ span: 9, offset: 0 }} lg={{ span: 6, offset: 1 }}>
					{id}
				</RowItem>
				<RowItem md={{ span: 5, offset: 0 }} lg={{ span: 4, offset: 2 }}>
					{amount / 100} {currency} (Stripe)
				</RowItem>
				<RowItem md={{ span: 4, offset: 0 }} lg={{ span: 4, offset: 1 }}>
					**** **** **** {payment_method_details.card.last4}
				</RowItem>
				<RowItemRight md={{ span: 3, offset: 0 }} lg={{ span: 2, offset: 1 }}>
					<a href={receipt_url} target="_blank" rel="noopener noreferrer">
						view details
					</a>
				</RowItemRight>
			</ChargeRecordWrapper>
		</ChargesStyle>
	);
};
