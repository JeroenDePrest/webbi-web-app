import { AccountpageService } from '@api/accountPage';
import { WebsiteDetailInfoProps } from '@components/WebsiteDetail/websiteDetailInfo/websiteDetailInfoContainer';
import {
    Active,
    Cancelled,
    Inactive,
    Actions,
    Tag,
    WebsiteDetailInfoStyle,
    ButtonWrapper,
} from '@components/WebsiteDetail/websiteDetailInfo/websiteDetailInfoStyle';
import { Button, Col, Icon, Modal, Row } from 'antd';
import { autobind } from 'core-decorators';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { ICard } from 'stripe';

const { confirm } = Modal;
interface State {
    renew: boolean;
    loading: boolean;
}

@autobind
export class WebsiteDetailInfo extends React.Component<WebsiteDetailInfoProps, State> {
    public readonly state: State = {
        renew: false,
        loading: false,
    };

    public render() {
        return (
            <WebsiteDetailInfoStyle>
                <Row>
                    <Col span={12}>
                        <Tag>Status</Tag>
                        {this.status()}
                    </Col>
                    <Col span={12}>
                        <Actions>
                            <Link target="_blank" to={`/user/${this.props.accountInfo.username}`}>
                                <ButtonWrapper>
                                    <Icon type="eye" /> View website
                                </ButtonWrapper>
                            </Link>

                            <ButtonWrapper type="primary" onClick={this.editWebsite}>
                                <Icon type="edit" /> Edit website
                            </ButtonWrapper>
                        </Actions>
                    </Col>
                </Row>
            </WebsiteDetailInfoStyle>
        );
    }

    private status() {
        const { accountInfo } = this.props;
        if (accountInfo.active && !accountInfo.isCancelled) {
            return <Active>Online</Active>;
        } else if (accountInfo.active && accountInfo.isCancelled) {
            return (
                <Cancelled>
                    <span>Cancelled</span>
                    <ButtonWrapper
                        loading={this.state.loading}
                        className="btn-renew"
                        onClick={this.reactivate}>
                        <Icon type="retweet" />
						Re-activate
                    </ButtonWrapper>
                </Cancelled>
            );
        } else {
            return (
                <Inactive>
					Offline
                    <Button
                        loading={this.state.loading}
                        className="btn-renew"
                        onClick={this.showConfirm}>
						Renew
                    </Button>
                </Inactive>
            );
        }
    }

    private showConfirm() {
        const { accountInfo, getAccountInfo, getBillingInfo, billingInfo } = this.props;
        const setLoading = () => this.setState({ loading: true });
        const disableLoading = () => this.setState({ loading: false });
        if (billingInfo && billingInfo.customer) {
            confirm({
                width: 500,
                title: 'This will charge your card',
                content: `By accepting a new charge will be made to the card ending in ${
                    (billingInfo.customer.default_source as ICard).last4
                }`,
                okText: 'Yes, renew my website',
                okType: 'primary',
                cancelText: 'No, keep my website offline',
                maskClosable: true,
                async onOk() {
                    setLoading();
                    await AccountpageService.renewWebsite(accountInfo.instagramId).then(() => {
                        getBillingInfo();
                        getAccountInfo();
                    });
                    disableLoading();
                },
            });
        }
    }

    private async reactivate() {
        const { accountInfo } = this.props;
        this.setState({ loading: true });
        await AccountpageService.renewWebsite(accountInfo.instagramId);
        this.props.getBillingInfo();
        this.props.getAccountInfo();
        this.setState({ loading: false });
    }

    private editWebsite() {
        const { accountInfo, setEdit, history } = this.props;
        setEdit(true, accountInfo.template);

        history.push(`/configurator/${accountInfo.instagramId}?template=${accountInfo.template}`);
    }
}
