import { AccountInfo } from '@api/accountPage';
import { BillingInfo } from '@api/charge';
import { Template } from '@api/templates';
import { WebsiteDetailInfo } from '@components/WebsiteDetail/websiteDetailInfo/websiteDetailInfo';
import { selectAccount } from '@store/actions/configurator/selectAccount';
import { setEdit } from '@store/actions/configurator/setEdit';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose, Dispatch } from 'redux';
import { selectTemplate } from '@store/actions/configurator/selectTemplate';

export interface WebsiteDetailInfoContainerProps {
	accountInfo: AccountInfo;
	billingInfo?: BillingInfo;
	getAccountInfo(): void;
	getBillingInfo(): void;
}

export const mapStateToProps = (state: StoreState) => {
	return {};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		selectAccount: (account: InstagramBusinessAccount) => dispatch(selectAccount({ account })),
		selectTemplate: (template: Template) => dispatch(selectTemplate({ template })),
		setEdit: (inConfigurationProcess: boolean, template?: string) =>
			dispatch(setEdit({ inConfigurationProcess, template })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const WebsiteDetailInfoContainer = compose<
	React.ComponentType<WebsiteDetailInfoContainerProps>
>(
	withRedux,
	withRouter
)(WebsiteDetailInfo);

export type WebsiteDetailInfoProps = WebsiteDetailInfoContainerProps &
	StateProps &
	DispatchProps &
	RouteComponentProps;
