// tslint:disable:import-name
import styled from 'styled-components';
import { Col, Button } from 'antd';

export const WebsiteDetailInfoStyle = styled.div`
	background-color: white;
	padding: 24px 48px;
	margin: 0 0 40px;

	& .ant-col {
		height: 60px;
	}
`;

export const Active = styled(Col)`
	color: #28a745;
	font-size: 24px;

	& > .btn-renew {
		margin-left: 10px;
	}
`;

export const Cancelled = styled(Active)`
	color: #ffc108;
	display: flex;
`;

export const Inactive = styled(Active)`
	color: #ff4d4f;
`;

export const Tag = styled.div`
	color: rgba(0, 0, 0, 0.35);
`;

export const Actions = styled.div`
	padding-top: 14px;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: flex-end;
	& .ant-btn {
		margin-right: 10px;
	}
`;

export const ButtonWrapper = styled(Button)`
	display: flex;
	align-items: center;
`;
