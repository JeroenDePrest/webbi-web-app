import { StoreState } from '@store/reducers/root';
import { InjectedEditableModuleProps, makeEditableModule } from '@utils/hocs/editableModuleHoc';
import { ITextBlockConfig, TextBlock, textBlockConfig, TextBlockProps as TextBlockModuleProps } from '@webbi/modules';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

export interface TextBlockContainerProps extends TextBlockModuleProps { }

export const mapStateToProps = (state: StoreState) => {
	return {
		inConfigurationProcess: state.configurator.inConfigurationProcess,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const TextBlockContainer = compose<React.ComponentType<TextBlockContainerProps>>(
	withRedux,
	makeEditableModule<ITextBlockConfig>(textBlockConfig)
)(TextBlock);

export type TextBlockProps = TextBlockContainerProps &
	StateProps &
	DispatchProps &
	InjectedEditableModuleProps<ITextBlockConfig>;
