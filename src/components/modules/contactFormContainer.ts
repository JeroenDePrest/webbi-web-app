import { StoreState } from '@store/reducers/root';
import { InjectedEditableModuleProps, makeEditableModule } from '@utils/hocs/editableModuleHoc';
import { ContactForm, contactFormConfig, ContactFormProps as ContactFormModuleProps, IContactFormConfig } from '@webbi/modules';
import { Form } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

export interface ContactFormContainerProps extends ContactFormModuleProps { }

export const mapStateToProps = (state: StoreState) => {
	return {
		inConfigurationProcess: state.configurator.inConfigurationProcess,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

const withForm = Form.create();

export const ContactFormContainer = compose<React.ComponentType<ContactFormContainerProps>>(
	withRedux,
	withForm,
	makeEditableModule<IContactFormConfig>(contactFormConfig),
)(ContactForm);

export type ContactFormProps = ContactFormContainerProps &
	StateProps &
	DispatchProps &
	FormComponentProps &
	InjectedEditableModuleProps<IContactFormConfig>;
