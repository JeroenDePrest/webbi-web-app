import { StoreState } from '@store/reducers/root';
import { InjectedEditableModuleProps, makeEditableModule } from '@utils/hocs/editableModuleHoc';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import { InstagramFeed, instagramFeedConfig, IInstagramFeedConfig, InstagramFeedProps as InstagramFeedModuleProps } from '@webbi/modules';

export interface InstagramFeedContainerProps extends InstagramFeedModuleProps {

}

export const mapStateToProps = (state: StoreState) => {
	return {
		inConfigurationProcess: state.configurator.inConfigurationProcess,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const InstagramFeedContainer = compose<React.ComponentType<InstagramFeedContainerProps>>(
	withRedux,
	makeEditableModule<IInstagramFeedConfig>(instagramFeedConfig)
)(InstagramFeed);

export type InstagramFeedProps = InstagramFeedContainerProps &
	StateProps &
	DispatchProps &
	InjectedEditableModuleProps<IInstagramFeedConfig>;
