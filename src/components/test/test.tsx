import * as React from 'react';
import { TestStyle } from '@components/test/testStyle';

export interface TestProps {}

export const Test: React.StatelessComponent<TestProps> = (props: TestProps) => {
	return <TestStyle />;
};
