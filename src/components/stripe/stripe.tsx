import React, { createContext, useContext, ReactNode } from 'react';
import { StripeProvider, injectStripe, Elements, ReactStripeElements } from 'react-stripe-elements';

const StripeContext = createContext<ReactStripeElements.StripeProps | undefined>(undefined);

interface StripeHookProviderInnerWrapperProps extends ReactStripeElements.InjectedStripeProps {
    stripeData: ReactStripeElements.StripeProps | undefined;
    children: ReactNode;
}

interface StripeHookProviderProps {
    apiKey: string;
    children: ReactNode;
}

export const StripeHookProvider: React.FC<StripeHookProviderProps> = (props) => {
    return (
        <StripeProvider apiKey={props.apiKey}>
            <Elements>
                <StripeInjectedWrapper stripeData={undefined}>{props.children}</StripeInjectedWrapper>
            </Elements>
        </StripeProvider>
    );
};

const StripeHookProviderInnerWrapper = (props: StripeHookProviderInnerWrapperProps) => {
    return <StripeContext.Provider value={props.stripe}>{props.children}</StripeContext.Provider>;
};

const StripeInjectedWrapper = injectStripe(StripeHookProviderInnerWrapper);

export const useStripe = () => {
    const stripeData = useContext(StripeContext);

    return stripeData;
};