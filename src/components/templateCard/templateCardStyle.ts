import styled from 'styled-components';
import { Button } from 'antd';

export const TemplateCardStyle = styled.div`
	width: 330px;
	height: 206px;
	overflow: hidden;
	/* height: 275px; */
	margin: 16px 12px 10px;
	border-radius: 10px;
	box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.12);
	/* box-shadow: 0px 15px 10px rgba(0,0,0,.05); */
	position: relative;
`;

export const TemplateCardSelectedStyle = styled(TemplateCardStyle)`
	border: 3px solid #5b86e5;
`;

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

export const TemplateName = styled.div`
	font-size: 18px;
	font-weight: 500;
	margin-left: 16px;
	text-transform: capitalize;
	color: rgba(0, 0, 0, 0.55);
`;

export const LargeButton = styled(Button)`
	height: 40px !important;
	padding: 0 15px !important;
	font-size: 17px !important;
`;

export const TemplateHover = styled.div`
	width: 330px;
	height: 206px;

	display: flex;
	justify-content: center;
	align-items: center;

	background-color: rgba(0, 0, 0, 0);

	position: absolute;
	top: 0px;
	left: 0px;

	transition: background-color 550ms;

	${LargeButton}:first-child {
		margin-right: 40px;
	}

	:hover {
		background-color: rgba(0, 0, 0, 0.4);
		cursor: pointer;
	}
`;

export const Thumbnail = styled.img`
	width: 100%;
	overflow: hidden;
`;
