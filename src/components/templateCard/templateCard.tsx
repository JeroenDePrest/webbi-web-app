import { TemplateCardProps } from '@components/templateCard/templateCardContainer';
import {
	LargeButton,
	TemplateCardSelectedStyle,
	TemplateCardStyle,
	TemplateHover,
	TemplateName,
	Thumbnail,
	Wrapper,
} from '@components/templateCard/templateCardStyle';
import { autobind } from 'core-decorators';
import * as React from 'react';

interface State {
	hover: boolean;
}

@autobind
export class TemplateCard extends React.Component<TemplateCardProps, State> {
	public readonly state: State = {
		hover: false,
	};

	public render() {
		const { template, selected } = this.props;

		const name = template.name.replace(/_/g, ' ');

		const Card = selected ? TemplateCardSelectedStyle : TemplateCardStyle;

		return (
			<Wrapper>
				<Card
					onClick={this.selectTemplate}
					onMouseEnter={() => this.setState({ hover: true })}
					onMouseLeave={() => this.setState({ hover: false })}>
					{this.state.hover || selected ? (
						<TemplateHover>
							<LargeButton onClick={this.selectTemplate} type="primary">
								Select
							</LargeButton>
							<LargeButton
								type="default"
								onClick={e => {
									e.preventDefault();
									e.stopPropagation();
									this.props.showDemo(template.name);
								}}>
								{/* <Link
									to={`/template/${template.name}`}
									target="_blank"
									rel="noopener noreferrer"> */}
								Demo
								{/* </Link> */}
							</LargeButton>
						</TemplateHover>
					) : null}

					<Thumbnail src={template.previewUrl} />
				</Card>
				<TemplateName>{name}</TemplateName>
			</Wrapper>
		);
	}

	private selectTemplate() {
		const { template } = this.props;

		this.props.selectTemplate(template);
		this.props.select(template);
		// this.props.history.push("/app/preview")
		// this.props.history.push("/app/checkout")
	}
}
