import { Template } from '@api/templates';
import { TemplateCard } from '@components/templateCard/templateCard';
import { selectTemplate } from '@store/actions/configurator/selectTemplate';
import { StoreState } from '@store/reducers/root';
import { History } from 'history';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

export interface TemplateCardContainerProps {
	history: History;
	template: Template;
	selected: boolean;
	showDemo(templateName: string): void;
	select(template: Template): void;
}

export const mapStateToProps = (state: StoreState) => {
	return {};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		selectTemplate: (template: Template) => dispatch(selectTemplate({ template })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const TemplateCardContainer = compose<React.ComponentType<TemplateCardContainerProps>>(
	withRedux
)(TemplateCard);

export type TemplateCardProps = TemplateCardContainerProps & StateProps & DispatchProps;
