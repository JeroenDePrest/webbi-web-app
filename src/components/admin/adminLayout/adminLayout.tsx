import { autobind } from 'core-decorators';
import * as React from 'react';

import { AdminLayoutProps, NavItem } from '@components/admin/adminLayout/adminLayoutContainer';
import {
	AdminLayoutStyle,
	Content,
	Header,
	Inner,
	Sider,
	Trigger,
	Wrapper,
} from '@components/admin/adminLayout/adminLayoutStyle';
import { AuthState } from '@store/reducers/authReducer';
import { Layout, Menu, Breadcrumb } from 'antd';

import { Link, NavLink } from 'react-router-dom';

interface State {
	collapsed: boolean;
}

@autobind
export class AdminLayout extends React.Component<AdminLayoutProps, State> {
	public static defaultProps: Partial<AdminLayoutProps> = {
		header: true,
		contained: true,
	};

	public readonly state: State = {
		collapsed: false,
	};

	public get theme() {
		return this.props.dark ? 'dark' : 'light';
	}
	public get logo() {
		return this.props.dark
			? require('@assets/images/logo_white.svg')
			: require('@assets/images/logo_black.svg');
	}

	public render() {
		const { sidenav, header } = this.props;

		const icon = this.state.collapsed ? 'menu-unfold' : 'menu-fold';

		return (
			<AdminLayoutStyle>
				{header && this.renderHeader()}

				{sidenav ? (
					<Layout>
						{this.renderSideNav()}
						<Layout>
							<Layout.Header>
								<Trigger type={icon} onClick={this.toggle} />
							</Layout.Header>
							{this.renderContent()}
						</Layout>
					</Layout>
				) : (
					this.renderContent()
				)}

				{/* <Layout.Footer style={{ textAlign: "center" }}>
					Ant Design ©2018 Created by Ant UED
				</Layout.Footer> */}
			</AdminLayoutStyle>
		);
	}

	private toggle() {
		this.setState({
			collapsed: !this.state.collapsed,
		});
	}

	private renderSideNav() {
		return (
			<Sider
				width={300}
				theme={this.theme}
				trigger={null}
				collapsible
				breakpoint="lg"
				onCollapse={this.toggle}
				collapsed={this.state.collapsed}>
				<Menu theme={this.theme} mode="inline" defaultSelectedKeys={['1']}>
					{this.renderNavItems(true, this.props.renderSideBarItems)}
				</Menu>
			</Sider>
		);
	}

	private renderContent() {
		const { contained, match } = this.props;

		return (
			<Content>
				<Inner contained={contained}>
					{match.url.split('/')[3] && (
						<Breadcrumb style={{ margin: '16px 0' }}>
							<Breadcrumb.Item>
								<Link to="/admin/websites">Overview</Link>
							</Breadcrumb.Item>

							<Breadcrumb.Item>{match.url.split('/')[3]}</Breadcrumb.Item>
						</Breadcrumb>
					)}
					{this.props.children}
				</Inner>
			</Content>
		);
	}

	private renderHeader() {
		const { contained } = this.props;

		return (
			<Header theme={this.theme}>
				<Inner contained={contained}>
					{/* <Logo src={this.logo} /> */}
					<Link to="/">Webbi</Link>
					<Wrapper>
						<Menu theme={this.theme} mode="horizontal">
							{this.renderNavItems(false, this.props.renderItems)}
						</Menu>
					</Wrapper>
				</Inner>
			</Header>
		);
	}

	private renderNavItems(
		withIcon = false,
		renderItems?: (isAuthenticated: boolean, user?: AuthState['user']) => NavItem[]
	) {
		const { user, isAuthenticated } = this.props;

		let navItems: NavItem[] = [];

		if (renderItems) {
			navItems = renderItems(isAuthenticated, user);
		}

		return navItems
			.filter(n => n.shouldRender)
			.map(i => {
				return (
					<Menu.Item className="side-menu-item" key={i.to}>
						{i.component ? (
							i.component
						) : (
							<NavLink to={i.to} exact={i.exact}>
								{withIcon && i.icon}
								{i.name}
							</NavLink>
						)}
					</Menu.Item>
				);
			});
	}
}
