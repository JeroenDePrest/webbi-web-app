import styled from 'styled-components';

export const ChangeBillingFormStyle = styled.div`
	& form > div:not(:first-child) {
		margin: 10px 0;
	}

	& .ant-btn {
		margin: 10px 0;
	}
`;
