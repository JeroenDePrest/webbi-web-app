import { autobind } from 'core-decorators';
import * as React from 'react';

import { ChangeBillingFormProps } from '@components/admin/changeBillingForm/changeBillingFormContainer';
import { ChangeBillingFormStyle } from '@components/admin/changeBillingForm/changeBillingFormStyle';
import { CardNumberElement, CardExpiryElement, CardCVCElement } from 'react-stripe-elements';
import { Button, message } from 'antd';
import { ChargeApiService } from '@api/charge';
interface State {
	loading: boolean;
}

const createOptions = (padding?: string) => {
	return {
		style: {
			base: {
				fontSize: window.innerWidth < 450 ? '14px' : '18px',
				color: '#424770',
				letterSpacing: '0.025em',
				fontFamily: 'Lato',
				'::placeholder': {
					color: '#aab7c4',
				},
				...(padding ? { padding } : {}),
			},
			invalid: {
				color: '#9e2146',
			},
		},
	};
};

@autobind
export class ChangeBillingForm extends React.Component<ChangeBillingFormProps, State> {
	public readonly state: State = {
		loading: false,
	};

	public render() {
		return (
			<ChangeBillingFormStyle>
				<form onSubmit={this.handleSubmit}>
					<div>
						Card number
						<CardNumberElement {...createOptions()} />
					</div>
					<div>
						Expiration date
						<CardExpiryElement {...createOptions()} />
					</div>
					<div>
						CVC
						<CardCVCElement {...createOptions()} />
					</div>
					<Button loading={this.state.loading} htmlType="submit" type="primary">
						Update
					</Button>
				</form>
			</ChangeBillingFormStyle>
		);
	}

	private async handleSubmit(ev: any) {
		ev.preventDefault();
		this.setState({ loading: true });
		if (this.props.stripe) {
			const token = await this.props.stripe.createToken();

			if (token.token) {
				const response = await ChargeApiService.updateCardInfo(
					this.props.instagramId,
					token.token.id
				);

				if (response.success) {
					message.success('Card has been updated');
					this.setState({ loading: false });
					this.props.closeDrawer();
				} else {
					message.error('Card couldn\'t be updated');
					this.setState({ loading: false });
				}
			} else {
				message.error('Stripe token couldn\'t be created');
			}
		} else {
			console.error('Stripe.js hasn\'t loaded yet.');
		}

		this.setState({ loading: false });
	}
}
