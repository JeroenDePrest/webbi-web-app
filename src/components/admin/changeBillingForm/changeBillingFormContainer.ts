import { compose } from 'redux';

import { ChangeBillingForm } from '@components/admin/changeBillingForm/changeBillingForm';
import { injectStripe, ReactStripeElements } from 'react-stripe-elements';

export interface ChangeBillingFormContainerProps {
	instagramId: string;
	closeDrawer(): void;
}

export const ChangeBillingFormContainer = compose<
	React.ComponentType<ChangeBillingFormContainerProps>
>(injectStripe)(ChangeBillingForm);

export type ChangeBillingFormProps = ChangeBillingFormContainerProps &
	ReactStripeElements.InjectedStripeProps;
