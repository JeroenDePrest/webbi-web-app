import { autobind } from 'core-decorators';
import * as React from 'react';

import { BillingPreviewProps } from '@components/billingPreview/billingPreviewContainer';
import { BillingPreviewStyle, Value, Title } from '@components/billingPreview/billingPreviewStyle';

interface State {}

@autobind
export class BillingPreview extends React.Component<BillingPreviewProps, State> {
	public render() {
		return (
			<BillingPreviewStyle>
				<Value>
					<h1>{this.props.totalValue}</h1>/m
				</Value>
				<Title>Total billing</Title>
			</BillingPreviewStyle>
		);
	}
}
