import styled from 'styled-components';

export const BillingPreviewStyle = styled.div`
	background-color: #ffffff;
	padding: 24px 28px;
	border-radius: 5px;
	margin-bottom: 12px;

	width: 250px;
`;

export const Value = styled.div`
	display: flex;
	align-items: baseline;

	& > h1 {
		margin-bottom: 10px;
	}
`;

export const Title = styled.div`
	font-weight: 600;
	color: rgba(0, 0, 0, 0.35);
`;
