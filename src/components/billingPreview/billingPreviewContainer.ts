import { compose } from 'redux';

import { BillingPreview } from '@components/billingPreview/billingPreview';

export interface BillingPreviewContainerProps {
	totalValue: number;
}

export const BillingPreviewContainer = compose<React.ComponentType<BillingPreviewContainerProps>>()(
	BillingPreview
);

export type BillingPreviewProps = BillingPreviewContainerProps;
