import * as React from 'react';

export interface ConditionalWrapperProps {
	condition: boolean;
	children: any;
	wrapper(children: React.ReactNode): any;
}

export const ConditionalWrapper: React.StatelessComponent<ConditionalWrapperProps> = React.memo(
	({ condition, wrapper, children }) => {
		return condition ? wrapper(children) : children;
	}
);
