import { ChargesList } from '@api/charge';
import { Charge } from '@components/charge/charge';
import {
	ChargeListStyle,
	ChargesListCard,
	IconWrapperLeft,
	IconWrapperRight,
	NavigationWrapper,
} from '@components/chargeList/chargeListStyle';
import { Loader } from '@components/loader/loader';
import * as React from 'react';

export interface ChargeListProps {
	charges?: ChargesList;
	navigateChargePage: Function;
	page: number;
	loading: boolean;
}

// tslint:disable:use-simple-attributes
export const ChargeList: React.StatelessComponent<ChargeListProps> = (props: ChargeListProps) => {
	return (
		<ChargeListStyle>
			<h3>Receipts</h3>
			<ChargesListCard>
				{props.charges &&
					props.charges.data.map((charge, index) => {
						return <Charge key={`charge-key-${index}`} charge={charge} />;
					})}
				<NavigationWrapper>
					<IconWrapperLeft
						disabled={props.page === 0 || props.loading ? true : false}
						type="left"
						onClick={() => (props.page > 0 ? props.navigateChargePage('prev') : null)}
					/>
					{props.loading ? <Loader /> : <span>{props.page}</span>}

					<IconWrapperRight
						disabled={(props.charges && !props.charges.has_more) || props.loading}
						type="right"
						onClick={() =>
							props.charges && props.charges.has_more
								? props.navigateChargePage('next')
								: null
						}
					/>
				</NavigationWrapper>
			</ChargesListCard>
		</ChargeListStyle>
	);
};
