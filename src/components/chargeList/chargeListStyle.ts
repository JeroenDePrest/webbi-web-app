// tslint:disable:import-name
import { Icon } from 'antd';
import styled from 'styled-components';

export interface IconCustomProps {
	disabled: boolean | undefined;
}

export const ChargeListStyle = styled.div``;

export const ChargesListCard = styled.div`
	background-color: #fff;
	padding: 24px 48px;
	border-radius: 5px;
`;

export const NavigationWrapper = styled.div`
	text-align: right;
	display: flex;
	align-items: center;
	justify-content: flex-end;
	margin-top: 20px;
`;

export const IconWrapper = styled(Icon)<IconCustomProps>`
	cursor: pointer;
	color: ${(p: IconCustomProps) => (p.disabled ? '#cac9c9' : 'inherit')};
`;

export const IconWrapperLeft = styled(IconWrapper)`
	margin-right: 10px;
`;

export const IconWrapperRight = styled(IconWrapper)`
	margin-left: 10px;
`;
