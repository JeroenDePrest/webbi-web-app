import { ConfiguratorSidebarProps } from '@components/configurator/configuratorSidebar/configuratorSidebarContainer';
import {
	ConfiguratorContentStyle,
	ConfiguratorSidebarStyle,
	ModuleSub,
	ChangeThemeWrapper,
} from '@components/configurator/configuratorSidebar/configuratorSidebarStyle';
import { Collapse, Button } from 'antd';
import { autobind } from 'core-decorators';
import { JSONSchema6 } from 'json-schema';
import * as React from 'react';
// tslint:disable-next-line:import-name
import Form from 'react-jsonschema-form';

interface State {
	activeKeyName?: string;
}

const log = (type: string) => console.log.bind(console, type);

@autobind
export class ConfiguratorSidebar extends React.Component<ConfiguratorSidebarProps, State> {
	public readonly state: State = {
		activeKeyName: 'a',
	};

	private form: Form<object> | null = null;

	public render() {
		return (
			<ConfiguratorSidebarStyle>
				{/* <ConfiguratorToggle onClick={this.props.closeModuleEditor}>
					<Icon type="arrow-right" />
				</ConfiguratorToggle> */}

				{this.renderConfigurationContent()}
			</ConfiguratorSidebarStyle>
		);
	}

	private renderConfigurationContent() {
		let uniqueId = '';
		if (this.props.selectedModule) {
			const { id, type } = this.props.selectedModule;
			uniqueId = `${id}-${type}`;
		}

		const { order } = this.props;

		return (
			<ConfiguratorContentStyle>
				<Collapse bordered={false} activeKey={uniqueId} accordion>
					{order.map(moduleId => {
						const m = this.getModule(moduleId);

						if (!m) {
							return null;
						}

						const schemaTitle = m.schema.title;
						const currentId = `${m.id}-${m.type}`;

						return (
							<Collapse.Panel
								key={currentId}
								header={
									// tslint:disable-next-line:react-a11y-event-has-role
									<div
										onClick={() => {
											if (uniqueId === currentId) {
												this.props.openModuleEditor('', m.type, m.schema);
											} else {
												this.props.openModuleEditor(m.id, m.type, m.schema);
											}
										}}>
										<div>{m.id}</div>
										<ModuleSub>{schemaTitle}</ModuleSub>
									</div>
								}>
								<Form<object>
									ref={r => (this.form = r)}
									schema={m.schema as JSONSchema6}
									formData={this.props.moduleConfig}
									onChange={({ formData }) => {
										// this.props.updateModuleConfiguration(m, formData);
										// Check if selected module
										const selectedModule = this.props.selectedModule;
										if (selectedModule) {
											const selectedKey = `${selectedModule.id}-${selectedModule.type}`;
											if (selectedKey === currentId) {
												this.props.updateModuleConfiguration(m, formData);
											}
										}
									}}
									onSubmit={this.props.closeModuleEditor}
									onError={log('errors')}
									key={m.id}>
									<div />
								</Form>
							</Collapse.Panel>
						);
					})}
					{/* <Collapse.Panel
						key={'change-theme-1'}
						header={
							<div onClick={this.handleChangeTheme} key={'change-theme-2'}>
								<div>Change theme</div>
								<ModuleSub>{'Choose another theme'}</ModuleSub>
							</div>
						}
					/> */}
					<ChangeThemeWrapper>

						<Button onClick={this.handleChangeTheme} key={'change-theme-2'} type="default">Change theme</Button>
					</ChangeThemeWrapper>

					{/*
																<div onClick={() => {
																	this.props.openModuleEditor(m.id, m.type, m.schema);
																}} key={m.id}>{m.id}</div>*/}
				</Collapse>
			</ConfiguratorContentStyle>
		);
	}

	// @decorate(memoize)
	private getModule(moduleId: string) {
		return this.props.modules.find(m => m.id === moduleId);
	}

	private handleChangeTheme() {
		const { history } = this.props;
		history.push('/templates');
	}
}
