import { ConfiguratorSidebar } from '@components/configurator/configuratorSidebar/configuratorSidebar';
import { closeModuleEditor } from '@store/actions/configurator/closeModuleEditor';
import { openModuleEditor } from '@store/actions/configurator/openModuleEditor';
import { updateModuleConfiguration } from '@store/actions/configurator/updateModuleConfiguration';
import { SelectedModule } from '@store/reducers/editorReducer';
import { StoreState } from '@store/reducers/root';
import { getConfigForModule } from '@utils/getConfigForModule';
import { JSONSchema6 } from 'json-schema';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose, Dispatch } from 'redux';

export interface ConfiguratorSidebarContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	let moduleConfig;

	if (state.editor.moduleSelected && state.editor.selectedModule) {
		const { id, type } = state.editor.selectedModule;

		moduleConfig = getConfigForModule(id, type, state.editor.moduleConfig);
	}

	return {
		moduleSelected: state.editor.moduleSelected,
		modules: state.editor.modules,
		selectedModule: state.editor.selectedModule,
		order: state.editor.order,
		moduleConfig,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch, ownProps: StateProps) => {
	return {
		closeModuleEditor: () => dispatch(closeModuleEditor({})),
		updateModuleConfiguration: (module: SelectedModule, data: object) =>
			dispatch(updateModuleConfiguration({ id: module.id, module, data })),
		openModuleEditor: (id: string, moduleType: string, schema: JSONSchema6) =>
			dispatch(openModuleEditor({ id, moduleType, schema })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const ConfiguratorSidebarContainer = compose<
	React.ComponentType<ConfiguratorSidebarContainerProps>
>(
	withRedux,
	withRouter
)(ConfiguratorSidebar);

export type ConfiguratorSidebarProps = ConfiguratorSidebarContainerProps &
	StateProps &
	RouteComponentProps &
	DispatchProps;
