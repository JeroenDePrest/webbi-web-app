import styled from 'styled-components';

export const ConfiguratorSidebarStyle = styled.div`
	background: #f9f9f9;
	/* width: 350px; */
	right: 0;
	height: 100%;
	top: 0;
	box-shadow: 5px 5px 30px rgba(0, 0, 0, 0.4);
	position: relative;
	z-index: 10;

	#root__title {
		display: none;
	}

	#root {
		font-size: 0.9rem;
		padding-top: 10px;
		> *:not(#root__title) {
			padding: 0 20px;
		}

		label {
			font-weight: 500;
			margin-bottom: 0.25rem;
			& + .field-description {
				line-height: 1;
				margin-bottom: 0.75rem;
			}
		}
		.field-description {
			font-size: 0.8rem;
			color: #9c9c9c;
		}

		label input[type="checkbox"] + span {
			margin-left: 5px;
		}
	}

	#root__description {
		display: none;
	}

	.ant-collapse-content-box {
		background: #f7f7f7;
		border-top: 1px solid #e0e0e0;
	}
`;

export const ConfiguratorContentStyle = styled.div`
	max-height: 100vh;
	overflow-y: scroll;

	form > div:not(.form-group) {
		padding: 0 20px;
	}
`;
export const ConfiguratorToggle = styled.a`
	&&&&& {
		background: #061327;
		color: #fff;
		width: 50px;
		height: 50px;
		display: flex;
		align-items: center;
		justify-content: center;
		position: absolute;
		margin-left: -50px;
		margin-top: 20px;
	}
`;
export const ModuleSub = styled.div`
	font-size: 0.8rem;
	color: #9c9c9c;
`;

export const ChangeThemeWrapper = styled.div`
	display:flex;
	justify-content:center;
	align-items:center;
	
	padding: 20px;
`;
