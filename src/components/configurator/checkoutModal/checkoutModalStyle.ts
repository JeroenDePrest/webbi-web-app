import styled from 'styled-components';
import { Col } from 'antd';

export const CheckoutModalStyle = styled.div`
`;

export const BillingInfoTitle = styled.div`
    font-size: 1.2rem;
    font-weight: 500;
    margin-bottom: 0.5rem;
`;

export const CheckoutTitle = styled.h3`
    /* font-size: 1.2rem;
    font-weight: 500; */
`;

export const Promo = styled.span`
    /* font-size: 1.2rem;
    font-weight: 500; */
    color: #73d13d;
`;

export const SubscriptionType = styled.div`
    display:flex;
    justify-content: center;
    margin-top: 0px;
    margin-bottom: 30px;
    
`;

export const PlanWrapper = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content: space-between;
    border-bottom: 1px solid rgba(0,0,0,.15);
    padding:10px 0;
    height:50px;
`;

export const TotalPricing = styled.div`
    padding: 10px 0;
    border-bottom: 1px solid rgba(0,0,0,.15);
    margin-bottom: 10px;
`;

export const PricingRow = styled.div`
    display:flex;
    flex-direction: row;
    align-items:center;
    justify-content: space-between;
`;

export const PricingRowMuted = styled(PricingRow)`
    color: rgba(0,0,0,.3);
`;

export const CouponWrapper = styled.div`
    
`;

export const CouponInput = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    .ant-form-item {
        margin-bottom: 0;
    }
`;

export const PlanImage = styled.div`
    background-color: #374C95;
    border-radius: 4px;
`;

export const PlanInfo = styled.div`
    
`;

export const PlanPrice = styled.div`
    
`;

export const BillingInfoSection = styled.div`
    margin-bottom: 1rem;
    &:last-of-type {
        margin-bottom: 2rem;
    }
    .ant-form-item {
        margin-bottom: 0.3rem;
    }
`;

export const ProductCol = styled(Col)`
    background-color: rgba(0, 0, 0, 0.08);
    /* box-shadow: inset 5px 0 10px -2px rgba(0,0,0,0.1); */
    padding: 24px;
    padding-right: 24px !important;
    padding-left: 24px !important;
`;

export const UserInfoCol = styled(Col)`
    padding: 24px;
    padding-right: 24px !important;
    padding-left: 24px !important;
`;

export const StripeElement = styled.div`
	flex-grow: 2;
	/* display: flex; */
`;

export const PaymentForm = styled.div`
	display: flex;
	align-items: center;

	@media (max-width: 750px) {
		flex-direction: column;
		align-items: stretch;
		justify-content: center;
		button {
			margin-top: 16px;
		}
	}

.StripeElement{
	border: 1px solid #d9d9d9;
	border-radius: 4px;
	padding: 8px 1rem;
}
`;