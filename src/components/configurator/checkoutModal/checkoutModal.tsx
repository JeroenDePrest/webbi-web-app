import { ModuleTemplateConfig } from '@api/accountPage';
import { CalculatedPrice, ChargeApiService } from '@api/charge';
import countries from '@assets/countries.json';
import { BillingInfoSection, BillingInfoTitle, CheckoutModalStyle, CheckoutTitle, CouponInput, CouponWrapper, PaymentForm, PlanInfo, PlanPrice, PlanWrapper, PricingRow, PricingRowMuted, ProductCol, StripeElement, SubscriptionType, TotalPricing, UserInfoCol } from '@components/configurator/checkoutModal/checkoutModalStyle';
import { Loader } from '@components/loader/loader';
import { useStripe } from '@components/stripe/stripe';
import { clearConfig } from '@store/actions/configurator/clearConfig';
import { Button, Col, Form, Input, message, Modal, Row, Select } from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import { FormItemProps } from 'antd/lib/form';
import FormItem from 'antd/lib/form/FormItem';
import { useFormik, yupToFormErrors } from 'formik';
import { get } from 'lodash';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router';
import { CardElement } from 'react-stripe-elements';
import Stripe from 'stripe';
import * as Yup from 'yup';
import './checkoutModal.less';
import { useDebouncedCallback } from 'use-debounce';


export interface CheckoutModalProps {
    templateConfig: ModuleTemplateConfig;
    instagramId: string;
    templateName: string;
    isVisible: boolean;
    handleClose(): void;
}
export interface BillingInfo {
    name: string;
    address: Stripe.IAddress;
    vat?: string;
    coupon?: string;
    plan?: Subscription;
}

const getErrorProps = (
    errors: object,
    path: string
): Partial<FormItemProps> => {
    const errorMessage = get(errors, path, '');
    return errorMessage
        ? {
            validateStatus: 'error',
            help: errorMessage,
        }
        : {};
};

const TextInput: FC<{
    path: string;
    placeholder: string;
    value?: string;
    formik: {
        errors: object;
        values: object;
        handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    };
}> = ({ path, placeholder, value, formik }) => {
    return (
        <Form.Item {...getErrorProps(formik.errors, path)}>
            <Input
                name={path}
                placeholder={placeholder}
                onChange={formik.handleChange}
                defaultValue={value || get(formik.values, path, '')}
            />
        </Form.Item>
    );
};

const PlanItem: FC<{
    plan: Stripe.plans.IPlan | undefined;
}> = ({ plan }) => {
    if (!plan) {
        return null;
    }
    return (
        <PlanWrapper>
            {/* <PlanImage><img src={require('@assets/images/webbi-net')} alt="The plan image" /></PlanImage> */}
            <PlanInfo>
                {plan.nickname}
            </PlanInfo>

            <PlanPrice><FormattedNumber style={'currency'} currency={'eur'} value={(plan.amount || 0) / 100} /></PlanPrice>
        </PlanWrapper>
    );
};

const billingInfoSchema = Yup.object({
    name: Yup.string().required(),
    address: Yup.object({
        line1: Yup.string().required(),
        line2: Yup.string(),
        // eslint-disable-next-line @typescript-eslint/camelcase
        postal_code: Yup.string().required(),
        state: Yup.string(),
        city: Yup.string().required(),
        country: Yup.string()
            .length(2)
            .required(),
    }).required(),
    vat: Yup.string(),
    coupon: Yup.string(),
});

enum Subscription {
    MONTHLY = 'month',
    YEARLY = 'year'
}

export const CheckoutModal: FC<CheckoutModalProps> = ({
    isVisible,
    templateName,
    instagramId,
    templateConfig,
    handleClose,
}) => {
    const [disabled, setDisabled] = useState(false);
    const [loading, setLoading] = useState(false);
    const [done, setDone] = useState(false);
    const [subscriptionType, setSubscriptionType] = useState(Subscription.MONTHLY);
    const [plans, setPlans] = useState<Stripe.plans.IPlan[]>([]);
    const [coupon, setCoupon] = useState<{ couponOpen: boolean; coupon?: string }>({ couponOpen: false });
    const [total, setTotal] = useState<CalculatedPrice>({
        price: 0,
        plan: '',
        tax: 0,
        taxRate: 0,
        discount: 0,
        total: 0,
    });

    const dispatch = useDispatch();
    const stripe = useStripe();

    const formik = useFormik<BillingInfo>({
        initialValues: {
            name: '',
            address: {
                line1: '',
            },
        },
        validationSchema: billingInfoSchema,
        validateOnChange: false,
        onSubmit: values => {
            console.log(JSON.stringify({ ...formik.values, coupon: coupon.coupon || '' }));

            setDisabled(true);
            setLoading(true);

            if (stripe) {
                stripe
                    .createToken({})
                    .then(({ token }) => {
                        if (!token) {
                            // TODO throw invalid token
                            throw new Error('Invalid card');
                        }

                        return ChargeApiService.chargeAccount(
                            token.id,
                            templateConfig,
                            instagramId,
                            templateName,
                            { ...formik.values, coupon: coupon.coupon || '', plan: subscriptionType }
                        );
                    })
                    .then(() => {
                        dispatch(clearConfig({}));
                        setDisabled(false);
                        setLoading(false);
                        setDone(true);
                    })
                    .catch((err: any) => {
                        setDisabled(false);
                        setLoading(false);
                        console.error(err);
                        message.error('Something went wrong');
                    });
            }
        },

    });

    const plan = plans.find((p: Stripe.plans.IPlan) => p.interval === subscriptionType);

    const [calculatedPriceDebounced] = useDebouncedCallback(
        // function
        async (coupon) => {
            if (plan) {
                try {
                    const calculatedPrice = await ChargeApiService.getCalculatedPrice({
                        plan: plan.interval,
                        countryCode: formik.values.address.country || '',
                        vat: formik.values.vat === '' ? undefined : formik.values.vat,
                        coupon: coupon || '',
                    });
                    setTotal(calculatedPrice);
                    formik.setFieldError('vat', undefined);
                } catch (err) {
                    console.log(err);
                    if (err.data && err.data.name === 'VALIDATION_ERROR') {
                        console.log(yupToFormErrors(err.data.validation));
                        formik.setErrors(yupToFormErrors(err.data.validation));
                    }
                }
            }
        },
        // delay in ms
        1000,
        { leading: true }
    );

    const getCalculatedPrice = useCallback(async (coupon = '') => {
        console.log('calculatedPrice');

        if (plan) {
            calculatedPriceDebounced(coupon);
        }
    }, [formik.values.address.country, formik.values.vat, plan]);

    useEffect(() => {
        async function getInitialData() {
            console.log('getInitialData', subscriptionType);
            try {
                const plansResponse = await ChargeApiService.getPlans();

                const selectedPlan = plansResponse.find((p: Stripe.plans.IPlan) => p.interval === subscriptionType);
                setPlans(plansResponse);
                if (selectedPlan) {
                    getCalculatedPrice();
                }
            } catch (error) {
                console.error(error);
            }
        }
        getInitialData();
    }, []);

    useEffect(() => {
        getCalculatedPrice();
    }, [getCalculatedPrice]);

    if (!templateName) {
        return <Redirect to="/templates" />;
    }

    if (done) {
        return <Redirect to="/success" />;
    }

    if (!plan) {
        return <Loader />;
    }


    return (
        <CheckoutModalStyle>
            <Modal
                className="checkout-modal"
                title={null}
                visible={isVisible}
                onCancel={handleClose}
                footer={null}
            >
                <Form
                    onSubmit={formik.handleSubmit}
                >

                    <Row type="flex">
                        <UserInfoCol span={15}>
                            <CheckoutTitle>Checkout</CheckoutTitle>
                            <BillingInfoSection>
                                <BillingInfoTitle>
                                    Billing info
                                </BillingInfoTitle>

                                <TextInput
                                    key="name"
                                    path="name"
                                    formik={formik}
                                    placeholder="Name"
                                />

                                <TextInput
                                    path="address.line1"
                                    formik={formik}
                                    placeholder="Street address/PO Box"
                                    value={formik.values.address.line1}
                                />

                                <TextInput
                                    path="address.line2"
                                    formik={formik}
                                    placeholder="Apartment/Suite/Unit/Building"
                                    value={formik.values.address.line2}
                                />

                                <Row gutter={10}>
                                    <Col span={16}>
                                        <TextInput
                                            path="address.city"
                                            formik={formik}
                                            placeholder="City"
                                            value={formik.values.address.city}
                                        />
                                    </Col>
                                    <Col span={8}>
                                        <TextInput
                                            path="address.postal_code"
                                            formik={formik}
                                            placeholder="Zip/Postal Code"
                                            value={
                                                formik.values.address
                                                    .postal_code
                                            }
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <TextInput
                                            path="address.state"
                                            formik={formik}
                                            placeholder="State/Province/County"
                                            value={formik.values.address.state}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <Form.Item
                                            {...getErrorProps(
                                                formik.errors,
                                                'address.country'
                                            )}
                                        >
                                            <Select
                                                showSearch
                                                placeholder="Country"
                                                optionFilterProp="children"
                                                onChange={async (value: string) => {
                                                    formik.setFieldValue(
                                                        'address.country',
                                                        value
                                                    );
                                                }}
                                            >
                                                {countries.map(country => (
                                                    <Select.Option
                                                        key={country['alpha-2']}
                                                        value={
                                                            country['alpha-2']
                                                        }
                                                    >
                                                        {country.name}
                                                    </Select.Option>
                                                ))}
                                            </Select>
                                        </Form.Item>
                                    </Col>


                                </Row>
                                <TextInput
                                    path="vat"
                                    formik={formik}
                                    placeholder="VAT (optional)"
                                    value={formik.values.vat}
                                />
                            </BillingInfoSection>

                            <BillingInfoSection>
                                <BillingInfoTitle>
                                    Card details
                                </BillingInfoTitle>
                                <PaymentForm>
                                    <StripeElement>
                                        <CardElement style={{
                                            base: {
                                                fontSize: '14px',
                                                color: '#000000a6',
                                                fontFamily: 'Open Sans, sans-serif',
                                                letterSpacing: '0.025em',
                                                '::placeholder': {
                                                    color: '#c3c3c3',
                                                },
                                            },
                                            invalid: {
                                                color: '#c23d4b',
                                            },
                                        }} />
                                    </StripeElement>
                                </PaymentForm>

                            </BillingInfoSection>
                            <Button
                                loading={loading}
                                disabled={disabled}
                                type="primary"
                                htmlType="submit"
                                size="large"
                            >
                                Pay now
                            </Button>
                        </UserInfoCol>
                        <ProductCol span={9}>
                            <SubscriptionType>
                                <ButtonGroup>
                                    <Button onClick={() => setSubscriptionType(Subscription.MONTHLY)} type={subscriptionType === Subscription.MONTHLY ? 'primary' : 'default'}>Monthly</Button>
                                    <Button onClick={() => setSubscriptionType(Subscription.YEARLY)} type={subscriptionType === Subscription.YEARLY ? 'primary' : 'default'}>Yearly {/*<Promo>2 months free</Promo>*/}</Button>
                                </ButtonGroup>
                            </SubscriptionType>
                            <PlanItem plan={plan} />
                            <TotalPricing>
                                <PricingRowMuted>
                                    <div>Sub Total (VAT excl.)</div>
                                    <div><FormattedNumber style={'currency'} currency={'eur'} value={(plan.amount || 0) / 100} /></div>
                                </PricingRowMuted>
                                <PricingRowMuted>
                                    <div>VAT ({total.taxRate || 0}%)</div>
                                    <div>{total.tax || total.tax > 0 ? <FormattedNumber style={'currency'} currency={'eur'} value={total.tax / 100} /> : '-'}</div>
                                </PricingRowMuted>
                                <PricingRowMuted>
                                    <div>Coupon</div>
                                    <div>{total.discount || total.discount > 0 ? <FormattedNumber style={'currency'} currency={'eur'} value={total.discount / 100} /> : '-'}</div>
                                </PricingRowMuted>
                                <PricingRow>
                                    <div>Total</div>
                                    <div><FormattedNumber style={'currency'} currency={'eur'} value={total.total / 100} /></div>
                                </PricingRow>
                            </TotalPricing>
                            <CouponWrapper>
                                {
                                    !coupon.couponOpen
                                        ? <div>Have a coupon code? <a href="#" onClick={() => setCoupon({ couponOpen: true })}>Enter</a></div>
                                        : <CouponInput>
                                            <FormItem>
                                                <input
                                                    type="text"
                                                    className="ant-input"
                                                    name="coupon"
                                                    onChange={(event) => {
                                                        setCoupon({
                                                            couponOpen: true,
                                                            coupon: event.target.value,
                                                        });
                                                    }}
                                                    placeholder="Coupon"
                                                />
                                            </FormItem>
                                            <Button onClick={() => {
                                                getCalculatedPrice(coupon.coupon);
                                            }} type="primary">Add Coupon</Button></CouponInput>
                                }
                            </CouponWrapper>
                        </ProductCol>
                    </Row>
                </Form>
            </Modal>
        </CheckoutModalStyle>
    );
};
