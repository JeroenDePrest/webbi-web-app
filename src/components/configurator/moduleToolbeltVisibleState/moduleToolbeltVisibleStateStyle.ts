import styled, { css } from 'styled-components';

export const ModuleToolbelt = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
	pointer-events: none;
	background: blue;
	width: 100%;
	z-index: 6;
`;

export interface ModuleToolbeltVisibleStateStyleProps {
	openInEditor: boolean;
}

const openToolbarCss = css`
    &:after{
        content:'';
        position:absolute;
        left:0;
        top:0;
        width:100%;
        height:100%;
        display:block;
        border:4px solid blue;
        /* margin:-4px; */
    }

    /* ${ModuleToolbelt}{
        opacity:1;
        pointer-events:all;
    } */
`;

export const ModuleToolbeltVisibleStateStyle = styled.div<ModuleToolbeltVisibleStateStyleProps>`
	position: relative;
	cursor: pointer;

	${({ openInEditor }) => openInEditor && openToolbarCss}

	&:hover {
		${openToolbarCss}
	}
`;
