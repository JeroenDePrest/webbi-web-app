import { ModuleToolbeltVisibleStateStyle } from '@components/configurator/moduleToolbeltVisibleState/moduleToolbeltVisibleStateStyle';
import * as React from 'react';

export interface ModuleToolbeltVisibleStateProps {
	children: React.ReactNode;
	openInEditor: boolean;

	openModuleEditor(): void;
}

export const ModuleToolbeltVisibleState: React.StatelessComponent<
	ModuleToolbeltVisibleStateProps
> = (props: ModuleToolbeltVisibleStateProps) => {
	return (
		<ModuleToolbeltVisibleStateStyle
			openInEditor={props.openInEditor}
			onClick={e => {
				e.preventDefault();
				props.openModuleEditor();
			}}>
			{props.children}
		</ModuleToolbeltVisibleStateStyle>
	);
};
