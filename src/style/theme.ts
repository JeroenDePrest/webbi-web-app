export const theme = {
	blue6: '#2D7CCE',
	primary: '#5B86E5',
	primaryDesaturated1: 'hsl(160, 44%, 87%)',
	fontFamilyHeader: 'PT Sans, sans-serif',
	fontWeightBold: 500,
	dark: '#212121',
	light: '#fbfbfb',
};
