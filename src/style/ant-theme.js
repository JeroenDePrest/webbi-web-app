// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

module.exports = {
    "blue-6": "#2D7CCE",
    "primary-color": "#5B86E5",
    // "primary-color": "linear-gradient(#36D1DC, #5B86E5)",
    "border-radius-base": "4px",
    "layout-header-background": "#212121",
    "font-family": "Lato"
};
