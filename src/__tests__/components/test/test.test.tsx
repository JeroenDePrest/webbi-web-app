import { Test } from '@components/test/test';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
	const wrapper = renderer.create(<Test />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
