import { BillingPreviewContainer } from '@components/billingPreview/billingPreviewContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<BillingPreviewContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
