import { AccountCardContainer } from '@components/accountCard/accountCardContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<AccountCardContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
