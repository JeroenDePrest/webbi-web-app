import { WebsiteDetailInfoContainer } from '@components/WebsiteDetail/websiteDetailInfo/websiteDetailInfoContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<WebsiteDetailInfoContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
