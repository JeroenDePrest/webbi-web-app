import { Charges } from '@components/charges/charges';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
	const wrapper = renderer.create(<Charges />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
