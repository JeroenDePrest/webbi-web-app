import { ChangeBillingFormContainer } from '@components/admin/changeBillingForm/changeBillingFormContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<ChangeBillingFormContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
