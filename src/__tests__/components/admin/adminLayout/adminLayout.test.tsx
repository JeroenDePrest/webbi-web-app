import { AdminLayoutContainer } from '@components/admin/adminLayout/adminLayoutContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<AdminLayoutContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
