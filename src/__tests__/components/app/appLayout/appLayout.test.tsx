import { AppLayoutContainer } from '@components/app/appLayout/appLayoutContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<AppLayoutContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
