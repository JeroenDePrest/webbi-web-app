import { ChargeList } from '@components/chargeList/chargeList';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
	const wrapper = renderer.create(<ChargeList />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
