import { TemplateCardContainer } from '@components/templateCard/templateCardContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<TemplateCardContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
