import { PublicPageContainer } from '@pages/publicPage/publicPageContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
	const wrapper = renderer.create(<PublicPageContainer />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
