import { AccountPickerContainer } from '@pages/accountPicker/accountPickerContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<AccountPickerContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
