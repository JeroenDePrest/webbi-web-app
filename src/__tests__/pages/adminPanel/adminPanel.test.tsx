import { AdminPanelContainer } from '@pages/adminPanel/adminPanelContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<AdminPanelContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
