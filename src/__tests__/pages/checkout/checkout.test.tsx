import { CheckoutContainer } from '@pages/checkout/checkoutContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<CheckoutContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
