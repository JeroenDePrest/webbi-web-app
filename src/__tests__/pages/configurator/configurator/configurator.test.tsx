import { ConfiguratorContainer } from '@pages/configurator/configurator/configuratorContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<ConfiguratorContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
