import { WebsiteDetailContainer } from '@pages/admin/websiteDetail/websiteDetailContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
	const wrapper = renderer.create(<WebsiteDetailContainer />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
