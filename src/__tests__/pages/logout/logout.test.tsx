import { LogoutContainer } from '@pages/logout/logoutContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<LogoutContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
