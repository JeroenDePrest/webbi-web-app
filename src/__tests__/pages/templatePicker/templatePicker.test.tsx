import { TemplatePickerContainer } from '@pages/templatePicker/templatePickerContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<TemplatePickerContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
