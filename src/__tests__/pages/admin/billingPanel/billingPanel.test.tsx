import { BillingPanelContainer } from '@pages/admin/billingPanel/billingPanelContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<BillingPanelContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
