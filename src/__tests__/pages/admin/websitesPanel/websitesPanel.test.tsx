import { WebsitesPanelContainer } from '@pages/admin/websitesPanel/websitesPanelContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<WebsitesPanelContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
