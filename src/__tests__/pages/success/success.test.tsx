import { SuccessContainer } from '@pages/success/successContainer';
import React from 'react';
import renderer from 'react-test-renderer';

it('should match snapshot', () => {
  const wrapper = renderer.create(<SuccessContainer />);
  const tree = wrapper.toJSON();
  expect(tree).toMatchSnapshot();
});
