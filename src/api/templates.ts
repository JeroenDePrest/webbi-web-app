import { authRequest } from './request';
import { ModuleTemplateConfig } from './accountPage';

export interface Template {
	name: string;
	previewUrl?: string;
}

export interface InstagramMedia {
	media_url: string;
	media_type: string;
	permalink: string;
	caption: string;
	id: string;
}

export interface Cursors {
	before: string;
	after: string;
}

export interface Paging {
	cursors: Cursors;
}

export interface Media {
	data: InstagramMedia[];
	paging: Paging;
}

export interface TemplateContext {
	name: string;
	biography: string;
	profile_picture_url: string;
	media_count: number;
	media: Media;
	instagramId: string;
	moduleConfigs?: ModuleTemplateConfig;
	originalTemplate?: string;
}

export class TemplateApiService {
	public static async getAllTemplates() {
		return authRequest<Template[]>({
			url: '/templates',
			method: 'GET',
		});
	}
}
