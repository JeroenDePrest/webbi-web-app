import Stripe from 'stripe';
import { authRequest, request } from './request';

export interface Website {
	active: boolean;
	endDate: number;
	instagramId: string;
	startDate: number;
	stripeSubscriptionId: string;
	template: string;
	templateConfig: ModuleTemplateConfig;
	userId: string;
	isCancelled: boolean;
	plan?: Stripe.plans.IPlan;
	username: string;
}

export interface AccountInfo {
	active: boolean;
	endDate: number;
	instagramId: string;
	startDate: number;
	stripeSubscriptionId: string;
	template: string;
	templateConfig: ModuleTemplateConfig;
	userId: string;
	isCancelled?: boolean;
	username: string;
}

export interface ModuleTemplateConfig {
	'instagram-feed'?: ModuleTemplateConfigItem;
	'contact-form'?: ModuleTemplateConfigItem;
	[template: string]: ModuleTemplateConfigItem | undefined;
}

export interface ModuleTemplateConfigItem {
	[id: string]: ModuleTemplateConfigValues;
}

export interface ModuleTemplateConfigValues {
	[key: string]: string | boolean | number;
}

export interface Module {
	input: ModuleInput;
	name: string;
}

export interface ModuleInput {
	default?: number;
	choices?: ModuleChoice[];
	label: string;
	name: string;
	type: string;
}

export interface ModuleChoice {
	label: string;
	value: string;
}

export class AccountpageService {
	public static async getAllWebsites() {
		return authRequest<Website[]>({
			url: '/page',
			method: 'GET',
		});
	}

	public static async cancelSubscription(instagramId: string) {
		return authRequest<any>({
			url: `/page/${instagramId}`,
			method: 'DELETE',
		});
	}
	public static async renewWebsite(instagramId: string) {
		return authRequest<any>({
			url: `/page/${instagramId}/renew`,
			method: 'GET',
		});
	}

	public static async getAccountInfo(instagramId: string) {
		return authRequest<AccountInfo>({
			url: `/page/${instagramId}`,
			method: 'GET',
		});
	}

	public static async updatePage(
		instagramId: string,
		data: {
			template: string;
			templateConfig: ModuleTemplateConfig;
		}
	) {
		return authRequest<Module[]>({
			url: `/page/${instagramId}`,
			method: 'PUT',
			data,
		});
	}

	public static async renderTemplate(instagramId: string, template?: string) {
		return authRequest<string>({
			url: `/page/user/${instagramId}`,
			method: 'GET',
			params: {
				template,
			},
		});
	}

	public static async renderDemoTemplate(instagramId: string, template: string) {
		return authRequest<string>({
			url: `/page/user/${instagramId}`,
			method: 'GET',
			params: {
				template,
				demo: true,
			},
		});
	}

	public static async renderTemplatePublic(username: string) {
		return request<string>({
			url: `/page/user/${username}/public`,
			method: 'GET',
		});
	}
}
