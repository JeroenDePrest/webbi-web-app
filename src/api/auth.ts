import { InstagramBusinessAccount } from '@store/reducers/authReducer';
import { authRequest } from './request';

export interface BaseAccountInfo {
	id: string;
	instagram_business_account: {
		id: string;
		username: string;
		profile_picture_url: string;
	};
	name: string;
}

export interface User {
	profile: {
		email: string;
		firstName: string;
		lastName: string;
		active: boolean;
	};
	accounts: InstagramBusinessAccount[];
}

interface Body {
	token: string;
}

export class AuthApiService {
	public static async login() {
		return authRequest<{ redirectUrl: string }>({
			url: `/auth?from=${window.location.protocol}//${window.location.host}/login/succes`,
			method: 'GET',
		});
	}

	public static async me() {
		return authRequest<User>({
			url: '/me',
			method: 'GET',
		});
	}

	public static async updateUser(user: User) {
		return authRequest<User>({
			url: '/me',
			method: 'PUT',
			data: {
				user,
			},
		});
	}
}
