// tslint:disable:import-name
import Stripe from 'stripe';
import { BillingInfo as CheckoutInfo } from '@components/configurator/checkoutModal/checkoutModal';
import { authRequest } from './request';
import { ModuleTemplateConfig } from './accountPage';

export interface Receipt {
    status: string;
}

export enum CouponType {
    PERCENTAGE = 'PERCENTAGE',
    AMOUNT = 'AMOUNT'
}

export interface Coupon {
    valid: boolean;
    type: CouponType;
    amount: number;
}

export interface BillingInfo {
    cardInfo: { brand: string; exp_month: number; exp_year: number; last4: string };
    subscription?: Stripe.subscriptions.ISubscription;
    charges?: ChargesList;
    customer?: Stripe.customers.ICustomer;
}
// tslint:disable:no-reserved-keywords
export interface BillingDetails {
    address: {
        city: string | null;
        country: string | null;
        line1: string | null;
        line2: string | null;
        postal_code: string | null;
        state: string | null;
    };
    email: string | null;
    name: string | null;
    phone: string | null;
}
export interface ChargeItem {
    amount: 299;
    amount_refunded: 0;
    balance_transaction: string;
    billing_details: BillingDetails;
    captured: boolean;
    created: number;
    currency: string;
    customer: string;
    description: string;
    id: string;
    invoice: string;
    livemode: boolean;
    metadata: any;
    object: string;
    outcome: {
        network_status: string;
        reason: string | null;
        risk_level: string;
        risk_score: number;
        seller_message: string;
        type: string;
    };
    paid: boolean;
    payment_intent: string;
    payment_method: string;
    payment_method_details: {
        card: {
            brand: string;
            checks: {
                address_line1_check: string | null;
                address_postal_code_check: string | null;
                cvc_check: string | null;
            };
            country: string;
            exp_month: number;
            exp_year: number;
            fingerprint: string;
            funding: string;
            last4: string;
            network: string;
        };
        type: string;
    };
    receipt_email: string | null;
    receipt_url: string;
    refunded: boolean;
    refunds: {
        data: any[];
        has_more: boolean;
        object: string;
        total_count: number;
        url: string;
    };
    source: {
        address_city: string | null;
        address_country: string | null;
        address_line1: string | null;
        address_line1_check: string | null;
        address_line2: string | null;
        address_state: string | null;
        address_zip: string | null;
        address_zip_check: string | null;
        brand: string;
        country: string;
        customer: string;
        cvc_check: string;
        exp_month: number;
        exp_year: number;
        fingerprint: string;
        funding: string;
        id: string;
        last4: string;
        metadata: {};
        name: string | null;
        object: string;
    };
    statement_descriptor: string;
    statement_descriptor_suffix: string | null;
    status: string;
}
export interface ChargesList {
    data: ChargeItem[];
    has_more: boolean;
    object: string;
    url: string;
}

export interface CalculatedPrice {
    price: number;
    plan: string;
    tax: number;
    taxRate: number;
    discount: number;
    total: number;
}

export class ChargeApiService {
    public static async chargeAccount(
        stripeToken: string,
        templateConfig: ModuleTemplateConfig,
        instagramId: string,
        template: string,
        billingInfo: CheckoutInfo
    ) {
        return authRequest<Receipt>({
            url: '/page',
            method: 'POST',
            data: {
                stripeToken,
                billingInfo,
                instagramId,
                template,
                templateConfig,
            },
        });
    }

    public static async getCoupon(coupon: string) {
        return authRequest<Coupon>({
            url: `/coupon/${coupon}`,
            method: 'GET',
        });
    }
    public static async updateBillingInfo(
        instagramId: string,
        address: Stripe.IAddress,
        name: string,
        vat: string
    ) {
        return authRequest<{ success: boolean }>({
            url: `/billing/${instagramId}`,
            method: 'POST',
            data: {
                address,
                name,
                vat,
            },
        });
    }

    public static async updateCardInfo(instagramId: string, token: string) {
        return authRequest<{ success: boolean }>({
            url: `/billing/${instagramId}/card`,
            method: 'POST',
            data: {
                stripeToken: token,
            },
        });
    }

    public static async getBillingInfo(instagramId: string) {
        return authRequest<BillingInfo>({
            url: `/billing/${instagramId}`,
            method: 'GET',
        });
    }

    public static async getCharges(
        instagramId: string,
        params: { before?: string; after?: string }
    ) {
        return authRequest<ChargesList>({
            url: `/billing/${instagramId}/charges`,
            params,
            method: 'GET',
        });
    }

    public static async getPlans() {
        return authRequest<Stripe.plans.IPlan[]>({
            url: '/billing/plans',
            method: 'GET',
        });
    }

    public static async getCalculatedPrice(body: { plan: string; countryCode: string; vat?: string; coupon: string }) {
        return authRequest<CalculatedPrice>({
            url: '/billing/calculateprice',
            method: 'POST',
            data: {
                ...body,
            },
        });
    }
}
