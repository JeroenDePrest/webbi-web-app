import { AuthService } from '@utils/auth/authService';
import { API_URL } from '@utils/env';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

/**
 * Create an Axios Client with defaults
 */
const client = axios.create({
	baseURL: API_URL,
});

/**
 * Request Wrapper with default success/error actions
 */
export const request = async <T = any>(options: AxiosRequestConfig) => {
	const onSuccess = (response: AxiosResponse<T>) => {
		console.debug('Request Successful!', response);

		return response.data;
	};

	const onError = async (error: AxiosError) => {
		console.error('Request Failed:', error.config);

		if (error.response) {
			// Request was made but server responded with something
			// other than 2xx
			console.error('Status:', error.response.status);
			console.error('Data:', error.response.data);
			console.error('Headers:', error.response.headers);

			if (error.response.status === 401) {
				AuthService.logout();
				window.location.replace('/');
			}
		} else {
			// Something else happened while setting up the request
			// triggered the error
			console.error('Error Message:', error.message);
		}

		return Promise.reject(error.response || error.message);
	};

	return client
		.request<T>(options)
		.then(onSuccess)
		.catch(onError);
};

export const authRequest = async <T>(options: AxiosRequestConfig) => {
	return request<T>({
		...options,
		headers: {
			...options.headers,
			Authorization: `Bearer ${localStorage.getItem('token')}`,
		},
	});
};
