import { AccountpageService, Website } from '@api/accountPage';
import { AccountCardContainer } from '@components/accountCard/accountCardContainer';
import { Loader } from '@components/loader/loader';
import { AccountPickerProps } from '@pages/accountPicker/accountPickerContainer';
import {
	AccountHeader,
	AccountPickerStyle,
	Accounts,
} from '@pages/accountPicker/accountPickerStyle';
import { autobind } from 'core-decorators';
import * as React from 'react';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';

interface State {}

@autobind
export class AccountPicker extends React.Component<AccountPickerProps, State> {
	public readonly state: State = {};

	public componentDidMount() {
		this.props.setEdit(false);

		AccountpageService.getAllWebsites()
			.then((resp: Website[]) => {
				this.props.addWebsites(
					resp.reduce((acc: string[], w: Website) => [...acc, w.username], [])
				);
			})
			.catch(err => {
				console.error(err);
			});
	}

	public render() {
		const { accounts } = this.props;

		return (
			<AccountPickerStyle>
				<AccountHeader>
					<h1>Select your account</h1>
					<p>
						Please select one of your accounts below. If your account / page is not
						showing, make sure the page is a business account.
					</p>
				</AccountHeader>
				<Accounts>
					{accounts.length === 0 ? <Loader /> : accounts.map(this.renderAccount)}
				</Accounts>
			</AccountPickerStyle>
		);
	}

	private renderAccount(account: InstagramBusinessAccount, index: number) {
		return <AccountCardContainer history={this.props.history} account={account} key={index} />;
	}
}
