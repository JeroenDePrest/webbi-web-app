import styled from 'styled-components';

export const AccountPickerStyle = styled.div`

`;

export const AccountHeader = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    
    height: 300px;
    
    p{
        max-width: 700px;
        color: rgba(0,0,0, 0.55);
        text-align: center;
        line-height: 1.4;
    }
    
    @media (max-width: 576px) {
        h1{
            font-size: 24px!important;
        }
        
        p{
            font-size: 16px !important;
        }
	}

	@media (max-width: 768px) {
		h1{
            font-size: 28px;
        }
        
        p{
            font-size: 18px !important;
        }
	}
    
`;


export const Accounts = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;
