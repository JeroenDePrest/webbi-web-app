import { AccountPicker } from '@pages/accountPicker/accountPicker';
import { setEdit } from '@store/actions/configurator/setEdit';
import { addWebsites } from '@store/actions/general/addWebsites';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { compose, Dispatch } from 'redux';

export interface AccountPickerContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	return {
		accounts: state.auth.accounts,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		addWebsites: (websites: string[]) => dispatch(addWebsites({ websites })),
		setEdit: (inConfigurationProcess: boolean) => dispatch(setEdit({ inConfigurationProcess })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const AccountPickerContainer = compose<React.ComponentType<AccountPickerContainerProps>>(
	withRedux
)(AccountPicker);

export type AccountPickerProps = AccountPickerContainerProps &
	RouteComponentProps &
	StateProps &
	DispatchProps;
