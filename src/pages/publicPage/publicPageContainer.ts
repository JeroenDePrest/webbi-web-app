import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import { StoreState } from '@store/reducers/root';

import { PublicPage } from '@pages/publicPage/publicPage';
import { RouteComponentProps, withRouter } from 'react-router';

export interface PublicPageContainerProps {
	username?: string;
}

export const mapStateToProps = (state: StoreState) => {
	return {};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const PublicPageContainer = compose<React.ComponentType<PublicPageContainerProps>>(
	withRedux,
	withRouter
)(PublicPage);

export type PublicPageProps = RouteComponentProps<{ username?: string }> &
	PublicPageContainerProps &
	StateProps &
	DispatchProps;
