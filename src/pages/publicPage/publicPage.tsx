import { nextTick } from 'process';
import { AccountpageService } from '@api/accountPage';
import backgroundImage from '@assets/images/RectangleHero.png';
import webbiLogo from '@assets/images/webbi-net.svg';
import { PublicPageProps } from '@pages/publicPage/publicPageContainer';
import { BackgroundImageWrapper, Body, ButtonGroup, Container, ErrorDescription, ErrorPageStyle, ErrorTitle, Header, LogoWrapper, PublicPageStyle } from '@pages/publicPage/publicPageStyle';
import { updateHabitat } from '@utils/habitat/HabitatApp';
import { Button } from 'antd';
import { autobind } from 'core-decorators';
import * as React from 'react';

interface State {
	html: string;
	loading: boolean;
	isError: boolean;
	error: any;
}

@autobind
export class PublicPage extends React.Component<PublicPageProps, State> {
	public readonly state: State = {
		html: '',
		loading: true,
		isError: false,
		error: null,
	};

	public componentDidMount() {
		const username = this.getUsername();

		if (username) {
			AccountpageService.renderTemplatePublic(username)
				.then(html => {
					this.setState({ html });
					nextTick(() => {
						this.setState({ loading: false }, () => {
							updateHabitat();
						});
						const scr = document && document.getElementById('myScript');
						const script = scr && scr.innerHTML;
						if (script) {
							(window as any).eval(script);
						}
					});
				})
				.catch((err: any) => {
					this.setState({ isError: true, error: err });
					// tslint:disable-next-line:no-console
					console.error({ err });
				});
		}
	}

	public goToWebbi() {
		window.location.href = 'https://www.webbi.co';
	}

	// tslint:disable:react-no-dangerous-html
	public render() {
		const { html, isError, error } = this.state;

		// if (loading) {
		// 	return <Spin />;
		// }

		if (isError) {
			return (
				<ErrorPageStyle>
					<BackgroundImageWrapper>
						<img src={backgroundImage} alt="rectangleHero"></img>
					</BackgroundImageWrapper>
					<Container className="container">
						<Header>
							<LogoWrapper onClick={() => this.goToWebbi()}>
								<img src={webbiLogo} alt="webboLogo"></img>
							</LogoWrapper>
							<span>Webbi</span>
						</Header>

						<Body>
							<ErrorTitle>We were not able to find an active website for this user</ErrorTitle>

							<ErrorDescription>
								{
									error.status === 404 ? (
										<div>
											If you are the owner of this account, you can create or reactivate this website. If the problem persists, please contact us.
									</div>
									) : (
											<div>An error occured.</div>
										)
								}
							</ErrorDescription>

							<ButtonGroup>
								<Button type="primary" size="large" href="https://webbi.co">Login</Button>
								<Button type="primary" size="large" href="mailto:support@webbi.co" ghost>Contact us</Button>
							</ButtonGroup>
						</Body>
					</Container>
				</ErrorPageStyle>
			);
		}

		return (
			<PublicPageStyle>
				<div id="htmlArea" dangerouslySetInnerHTML={{ __html: html }} />
			</PublicPageStyle>
		);
	}

	private getUsername() {
		const {
			match: {
				params: { username: routeUsername },
			},
			username,
		} = this.props;

		return routeUsername || username;
	}
}
