// tslint:disable-next-line:import-name
import styled from 'styled-components';
import { Layout, Button } from 'antd';

export const PublicPageStyle = styled.div``;

export const ErrorPageStyle = styled(Layout.Content)`
	color: white;
`;

export const Header = styled.div`
	display: flex;
	padding-top: 16px;
	span {
		font-size: 20px;
		color: white;
	}
`;

export const Body = styled.div`
	max-width: 900px;
	margin: 0 auto;
	margin-top: 192px;
`;

export const LogoWrapper = styled.div`
	margin-right: 6px;
	cursor: pointer;
`;

export const BackgroundImageWrapper = styled.div`
	display: block;
	max-width: 100%;
	position: absolute;
	min-width: 100%;
	overflow: hidden;
	height: 100%;
	z-index: -1;

	img {
		position: absolute;
		top: -800px;
		left: -200px;
		height: 1600px;
		right: 0;
		max-width: 2400px;
		z-index: -1;
		transform: rotate(-5deg);
		user-drag: none;
		user-select: none;
		-moz-user-select: none;
		-webkit-user-drag: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
`;

export const Container = styled.div`
	z-index: 2;
    max-width: 900px;
    margin: 0 auto;
`;

export const ErrorTitle = styled.h1`
	color: white;
	max-width:75%;
	margin-top: 36px;
`;

export const ErrorDescription = styled.div`
	max-width:70%;
	color: white;
	font-size: 1.3rem;
	a {
		color: white !important;
		text-decoration: underline !important;
	}
`;

export const ButtonGroup = styled.div`
	margin-top:2rem;
	.ant-btn:not(:last-child){
		margin-right:1rem;
	}
`;
