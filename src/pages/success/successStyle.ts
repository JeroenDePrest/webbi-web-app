import styled from 'styled-components';

export const SuccessStyle = styled.div`
    height: 300px;
    
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    
    i{
        font-size: 60px;
    }
    
`;
