import { autobind } from 'core-decorators';
import * as React from 'react';

import { SuccessProps } from '@pages/success/successContainer';
import { SuccessStyle } from '@pages/success/successStyle';
import { Icon } from 'antd';
import { Redirect } from 'react-router';

interface State {
	redirect: boolean;
}

@autobind
export class Success extends React.Component<SuccessProps, State> {
	public readonly state: State = {
		redirect: false,
	};

	componentDidMount() {
		setTimeout(() => this.setState({ redirect: true }), 2000);
	}

	public render() {
		if (this.state.redirect) {
			return <Redirect to="/" />;
		}

		return (
			<SuccessStyle>
				{/* <Icon type="setting" spin theme="filled" /> */}
				<Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
				<h1>Order Completed</h1>
				<p>We are building your website. We will send you an e-mail with the url.</p>
			</SuccessStyle>
		);
	}
}
