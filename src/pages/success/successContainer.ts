import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import { StoreState } from '@store/reducers/root';

import { Success } from '@pages/success/success';

export interface SuccessContainerProps {

}

export const mapStateToProps = (state: StoreState) => {
  return {

  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
  return {

  };
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(mapStateToProps, mapDispatchToProps);

export const SuccessContainer = compose<React.ComponentType<SuccessContainerProps>>(
 withRedux
)(Success);

export type SuccessProps =
  SuccessContainerProps &
  StateProps &
  DispatchProps;
