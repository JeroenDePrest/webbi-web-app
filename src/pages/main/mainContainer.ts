import { Main } from '@pages/main/main';
import { login } from '@store/actions/auth/login';
import { selectAccount } from '@store/actions/configurator/selectAccount';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { compose, Dispatch } from 'redux';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';

export interface MainContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	return {
		isAuthenticated: state.auth.isAuthenticated,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		login: (user: any) => dispatch(login({ user })),
		selectAccount: (account: InstagramBusinessAccount) => dispatch(selectAccount({ account })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const MainContainer = compose<React.ComponentType<MainContainerProps>>(withRedux)(Main);

export type MainProps = MainContainerProps & RouteComponentProps & StateProps & DispatchProps;
