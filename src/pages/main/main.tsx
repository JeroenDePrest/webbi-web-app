import { AuthApiService } from '@api/auth';
import { AccountpageService } from '@api/accountPage';
import { Loader } from '@components/loader/loader';
import { MainProps } from '@pages/main/mainContainer';
import { MainHeader, MainStyle } from '@pages/main/mainStyle';
import { autobind } from 'core-decorators';
import * as React from 'react';

interface State {
	loading: boolean;
}

@autobind
export class Main extends React.Component<MainProps, State> {
	public readonly state: State = {
		loading: false,
	};

	public componentDidMount() {
		if (this.props.isAuthenticated) {
			AccountpageService.getAllWebsites()
				.then(websites => {
					if (websites.length) {
						this.props.history.push('/admin/websites');
					} else {
						this.props.history.push('/accounts');
					}
				})
				.catch(err => {
					console.error(err);
				});
		} else {
			this.onClick();
		}
	}

	public onClick() {
		AuthApiService.login()
			.then(({ redirectUrl }) => {
				if (redirectUrl) {
					window.location.assign(redirectUrl);
				}
			})
			.catch(err => {
				console.error(err);
			});
	}

	public render() {
		if (this.props.isAuthenticated) {
			return <Loader />;
		}

		return (
			<MainStyle>
				<MainHeader>
					<h3>We are logging you in.</h3>
					<Loader />
				</MainHeader>
			</MainStyle>
		);
	}
}
