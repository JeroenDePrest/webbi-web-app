import styled from 'styled-components';

export const MainStyle = styled.div`
	@media (max-width: 576px) {
		h1 {
			font-size: 24px !important;
		}

		p {
			font-size: 16px !important;
		}
	}

	@media (max-width: 768px) {
		h1 {
			font-size: 28px;
		}

		p {
			font-size: 18px !important;
		}
	}
`;

export const MainHeader = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

	height: 300px;
	margin: 20px 0;

	p {
		max-width: 700px;
		color: rgba(0, 0, 0, 0.55);
		text-align: center;
		line-height: 1.4;
		margin-bottom: 20px;
	}
`;
