import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import { StoreState } from '@store/reducers/root';

import { WebsiteDetail } from '@pages/admin/websiteDetail/websiteDetail';
import { withRouter, RouteComponentProps } from 'react-router';

export interface WebsiteDetailContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	return {
		accounts: state.auth.accounts,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const WebsiteDetailContainer = compose<React.ComponentType<WebsiteDetailContainerProps>>(
	withRedux,
	withRouter
)(WebsiteDetail);

export type WebsiteDetailProps = WebsiteDetailContainerProps &
	StateProps &
	DispatchProps &
	RouteComponentProps<{ username: string }>;
