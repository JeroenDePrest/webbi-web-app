import { AccountInfo, AccountpageService } from '@api/accountPage';
import { BillingInfo, ChargeApiService } from '@api/charge';
import { ChargeList } from '@components/chargeList/chargeList';
import { Loader } from '@components/loader/loader';
import { WebsiteDetailInfoContainer } from '@components/WebsiteDetail/websiteDetailInfo/websiteDetailInfoContainer';
import { WebsiteDetailProps } from '@pages/admin/websiteDetail/websiteDetailContainer';
import { WebsiteDetailStyle } from '@pages/admin/websiteDetail/websiteDetailStyle';
import { autobind } from 'core-decorators';
import _ from 'lodash';
import * as React from 'react';
import { BillingPanelContainer } from '../billingPanel/billingPanelContainer';

interface State {
    accountInfo?: AccountInfo;
    cardInfo?: BillingInfo;
    chargePage: number;
    loading: boolean;
    charges: any;
}

@autobind
export class WebsiteDetail extends React.Component<WebsiteDetailProps, State> {
    public readonly state: State = {
        chargePage: 0,
        loading: false,
        charges: null,
    };

    public async componentDidMount() {
        if (this.props.accounts.length) {
            this.setState({ loading: true });
            await this.getAccountInfo();
            await this.getCreditCardInfo();
            this.setState({ loading: false });
        }
    }

    public async componentDidUpdate(prevProps: WebsiteDetailProps, prevState: State) {
        if (!_.isEqual(this.props.accounts, prevProps.accounts)) {
            this.setState({ loading: true });
            await this.getAccountInfo();
            await this.getCreditCardInfo();
            this.setState({ loading: false });
        }
    }
    // tslint:disable:use-simple-attributes
    public render() {
        const { accountInfo } = this.state;

        const account = this.getAccount();

        if (!accountInfo || !account) {
            return <Loader />;
        }


        return (
            <WebsiteDetailStyle>
                <h2>@{accountInfo.username}</h2>

                <WebsiteDetailInfoContainer
                    getAccountInfo={this.getAccountInfo}
                    getBillingInfo={this.getCreditCardInfo}
                    accountInfo={accountInfo}
                    billingInfo={this.state.cardInfo}
                />
                <>
                    <BillingPanelContainer
                        instagramId={account.instagramId}
                        getAccountInfo={this.getAccountInfo}
                        getBillingInfo={this.getCreditCardInfo}
                        accountInfo={accountInfo}
                        billingInfo={this.state.cardInfo}
                        accountActive={accountInfo.active}
                    />
                    <ChargeList
                        loading={this.state.loading}
                        charges={this.state.charges}
                        navigateChargePage={this.navigateChargePage}
                        page={this.state.chargePage}
                    />
                </>

            </WebsiteDetailStyle>
        );
    }

    public async getAccountInfo() {
        const account = this.getAccount();

        if (account) {
            const accountInfo = await AccountpageService.getAccountInfo(account.instagramId);

            this.setState({
                accountInfo,
            });
        }
    }

    private getAccount() {
        return this.props.accounts.find(
            account => account.username === this.props.match.params.username
        );
    }

    // TODO refactor, this is hardcoded
    private navigateChargePage(move: 'next' | 'prev') {
        const { chargePage } = this.state;
        const account = this.getAccount();
        // debugger;
        this.setState({ loading: true });

        if (move === 'prev' && chargePage > 0 && account) {
            this.setState(
                prevState => ({
                    chargePage: prevState.chargePage - 1,
                }),
                async () => {
                    const charges = await ChargeApiService.getCharges(account.instagramId, {
                        before: this.state.charges.data[0].id,
                    });
                    // eslint-disable-next-line @typescript-eslint/camelcase
                    charges.has_more = true;

                    this.setState({ charges, loading: false });
                }
            );
        } else if (move === 'next' && account) {
            this.setState(
                prevState => ({
                    chargePage: prevState.chargePage + 1,
                }),
                async () => {
                    console.log(this.state.charges[9]);
                    console.log(this.state.charges);
                    const charges = await ChargeApiService.getCharges(account.instagramId, {
                        after: this.state.charges.data[9].id,
                    });
                    this.setState({ charges, loading: false });
                }
            );
        }
    }
    private async getCreditCardInfo() {
        const account = this.getAccount();

        if (account) {
            const cardInfo = await ChargeApiService.getBillingInfo(account.instagramId);

            console.log(cardInfo);

            this.setState({ cardInfo, charges: cardInfo.charges });
        }
    }
}
