import { Website, AccountpageService } from '@api/accountPage';
import { Loader } from '@components/loader/loader';
import { WebsitesPanelProps } from '@pages/admin/websitesPanel/websitesPanelContainer';
import {
	Actions,
	Active,
	Template,
	Username,
	WebsiteHeaderRow,
	WebsiteRow,
	WebsitesPanelStyle,
	Inactive,
	Cancelled,
	PreviewRow,
	EmptyState,
} from '@pages/admin/websitesPanel/websitesPanelStyle';
import { Button, Col, Icon } from 'antd';
import { autobind } from 'core-decorators';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { BillingPreviewContainer } from '@components/billingPreview/billingPreviewContainer';

interface State {
	websites: Website[];
}

@autobind
export class WebsitesPanel extends React.Component<WebsitesPanelProps, State> {
	public readonly state: State = {
		websites: [],
	};
	public componentDidMount() {
		AccountpageService.getAllWebsites()
			.then((resp: Website[]) => {
				this.setState({ websites: resp });
			})
			.catch(err => {
				console.error(err);
			});
	}

	public render() {
		return (
			<WebsitesPanelStyle>
				<PreviewRow>
					<BillingPreviewContainer
						totalValue={this.state.websites.reduce(
							(acc, website) =>
								website.plan ? acc + (website.plan.amount || 0) / 100 : acc,
							0
						)}
					/>
				</PreviewRow>
				<h2>Websites ({this.state.websites.length})</h2>
				<Button type="primary" onClick={() => this.props.history.push('/accounts')}>
					Create new
				</Button>
				<WebsiteHeaderRow>
					<Col span={5}>Name</Col>
					<Col span={5}>Template</Col>
					<Col span={5}>Status</Col>
					<Col span={5}>Billing</Col>
					<Actions span={4}>Actions</Actions>
				</WebsiteHeaderRow>
				{this.state.websites ? this.renderWebsites() : <Loader />}
			</WebsitesPanelStyle>
		);
	}

	private renderWebsites() {
		return this.state.websites.length > 0 ? this.state.websites.map((w: Website, index: number) => {
			return (
				<WebsiteRow key={index}>
					<Link to={`/admin/websites/${w.username}`}>
						<Username md={5} sm={24}>
							@{w.username}
						</Username>
						<Template md={5} sm={24}>
							{w.template.replace(/_/g, ' ')}
						</Template>
						{this.status(w)}
						<Col span={5} style={{ height: '21px' }}>
							€{w.plan ? ((w.plan.amount || 0) / 100).toFixed(2) : '0.00'}/m
						</Col>
					</Link>

					<Actions md={4} sm={24}>
						<Link target="_blank" replace={false} to={`/user/${w.username}`}>
							<Icon type="eye" />
						</Link>

						<Icon type="edit" onClick={() => this.editWebsite(w)} />
						<Link to={`/admin/websites/${w.username}`}>
							<Icon type="delete" />
						</Link>
					</Actions>
				</WebsiteRow>
			);
		}) : <EmptyState><h2>No websites found</h2></EmptyState>;
	}

	private status(w: Website) {
		if (w.active && !w.isCancelled) {
			return (
				<Active md={5} sm={24}>
					Online
				</Active>
			);
		} else if (w.active && w.isCancelled) {
			return (
				<Cancelled md={5} sm={24}>
					Cancelled
				</Cancelled>
			);
		} else {
			return (
				<Inactive md={5} sm={24}>
					Offline
				</Inactive>
			);
		}
	}

	private editWebsite(website: Website) {
		this.props.setEdit(true, website.template);
		this.props.history.push(
			`/configurator/${website.instagramId}?template=${website.template}`
		);
	}
}
