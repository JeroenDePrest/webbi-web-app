import { Template } from '@api/templates';
import { WebsitesPanel } from '@pages/admin/websitesPanel/websitesPanel';
import { selectAccount } from '@store/actions/configurator/selectAccount';
import { selectTemplate } from '@store/actions/configurator/selectTemplate';
import { setEdit } from '@store/actions/configurator/setEdit';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose, Dispatch } from 'redux';

export interface WebsitesPanelContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	return {};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		selectAccount: (account: InstagramBusinessAccount) => dispatch(selectAccount({ account })),
		selectTemplate: (template: Template) => dispatch(selectTemplate({ template })),
		setEdit: (inConfigurationProcess: boolean, template?: string) =>
			dispatch(setEdit({ inConfigurationProcess, template })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const WebsitesPanelContainer = compose<React.ComponentType<WebsitesPanelContainerProps>>(
	withRedux,
	withRouter
)(WebsitesPanel);

export type WebsitesPanelProps = WebsitesPanelContainerProps &
	RouteComponentProps &
	StateProps &
	DispatchProps;
