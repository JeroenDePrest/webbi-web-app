import styled from 'styled-components';
import { Row, Col } from 'antd';

export const WebsitesPanelStyle = styled.div``;

export const WebsiteRow = styled(Row)`
	background-color: #ffffff;
	padding: 10px 16px;
	border-radius: 5px;

	margin-bottom: 12px;
	a {
		color: rgba(0, 0, 0, 0.65);
	}
`;

export const EmptyState = styled.div`
	display:flex;
	justify-content:center;
	h2{
		color: rgba(0,0,0,.45)
	}
`;

export const WebsiteHeaderRow = styled(Row)`
	padding: 16px;
	border-radius: 5px;

	@media (max-width: 770px) {
		display: none;
	}
`;

export const Active = styled(Col)`
	color: #28a745;
	& > .btn-renew {
		margin-left: 10px;
	}
`;

export const Inactive = styled(Col)`
	color: #ff4d4f;
`;

export const Cancelled = styled(Active)`
	color: #ffc108;
`;

export const Actions = styled(Col)`
	text-align: right;
	i {
		margin-left: 16px;
		&:hover {
			cursor: pointer;
		}
	}

	@media (max-width: 770px) {
		text-align: left;
		i {
			margin-right: 16px;
			margin-left: 0px;
		}
	}
`;

export const Username = styled(Col)`
	font-weight: 600;
	font-size: 16px;
`;

export const Template = styled(Col)`
	text-transform: capitalize;
`;

export const PreviewRow = styled.div`
	display: flex;
	margin-bottom: 20px;
`;
