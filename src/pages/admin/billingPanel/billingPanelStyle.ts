import styled from 'styled-components';
import { Col } from 'antd';

export const BillingPanelStyle = styled.div`
	margin-bottom: 40px;
	.ant-empty {
		margin: 20px 0;
	}
`;

export const BillingCard = styled.div`
	background-color: #fff;
	padding: 24px 48px;
	border-radius: 5px;
`;
export const Cost = styled.div`
	font-size: 200px;
	font-weight: 600;
	line-height: 160px;
`;

export const CostDecimals = styled.div`
	font-size: 34px;
`;

export const Period = styled.div`
	align-self: flex-end;
`;

export const CostWrapper = styled.div`
	display: flex;
`;

export const WebsitesOverview = styled.div`
	margin-top: 16px;
`;

export const CardInfoWrapper = styled.div`
	display: flex;
	flex-direction: column;
	height: 160px;

	.ant-btn-link {
		color: rgba(0, 0, 0, 0.65);
		&:hover {
			color: #f5222d;
			/* border-color: #f5222d; */
		}
		&:focus {
			color: #f5222d;
			/* border-color: #f5222d; */
		}
	}
`;

export const CardInfo = styled.div`
	height: 90px;
	margin-bottom: 20px;
`;

export const CardInfoTitle = styled.div`
	font-weight: 600;
	font-size: 24px;
`;

export const Buttons = styled.div``;

export const AccountData = styled.div`
	margin-bottom: 20px;
	height: 90px;
`;

export const Actions = styled(Col)`
	text-align: right;

	.ant-btn-dashed {
		&:hover {
			color: #f5222d;
			border-color: #f5222d;
		}
		&:focus {
			color: #f5222d;
			border-color: #f5222d;
		}
	}
`;
