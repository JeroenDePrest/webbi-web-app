import { AccountpageService } from '@api/accountPage';
import { ChargeApiService } from '@api/charge';
import { ChangeBillingFormContainer } from '@components/admin/changeBillingForm/changeBillingFormContainer';
import { Loader } from '@components/loader/loader';
import { BillingPanelProps } from '@pages/admin/billingPanel/billingPanelContainer';
import {
    AccountData,
    BillingCard,
    BillingPanelStyle,
    CardInfo,
    CardInfoTitle,
    CardInfoWrapper,
} from '@pages/admin/billingPanel/billingPanelStyle';
import { Button, Col, Drawer, Form, Input, message, Modal, Row, Select } from 'antd';
import { autobind } from 'core-decorators';
import moment from 'moment';
import * as React from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import { ICard } from 'stripe';
import countries from '@assets/countries.json';

const { confirm } = Modal;

interface State {
    changeCard: boolean;
    changeBilling: boolean;
    loading: boolean;
}

@autobind
export class BillingPanel extends React.Component<BillingPanelProps, State> {
    public readonly state: State = {
        changeCard: false,
        changeBilling: false,
        loading: false,
    };

    // tslint:disable-next-line: max-func-body-length cyclomatic-complexity
    public render() {
        const { billingInfo, accountInfo, accountActive } = this.props;
        const { form } = this.props;
        const { getFieldDecorator } = form;

        return (
            <BillingPanelStyle>
                <h3>Billing</h3>

                <BillingCard>
                    {billingInfo && billingInfo.customer ? (
                        <Row>
                            <Col span={12}>
                                <CardInfoTitle>Billing information</CardInfoTitle>
                                <AccountData>
                                    <strong>{`${billingInfo.customer.name}`}</strong>
                                    {billingInfo.customer.address ? (
                                        <div>
                                            <div>{billingInfo.customer.address.line1}</div>
                                            <div>{billingInfo.customer.address.line2}</div>
                                            <div>
                                                {`${billingInfo.customer.address.postal_code} ${billingInfo.customer.address.city}, ${billingInfo.customer.address.country}`}
                                            </div>
                                        </div>
                                    ) : (
                                            ''
                                        )}
                                </AccountData>
                                <Button onClick={() => this.setState({ changeBilling: true })}>
                                    Update Information
                                </Button>
                            </Col>
                            <Col span={12}>
                                {billingInfo.customer && billingInfo.customer.default_source ? (
                                    <CardInfoWrapper>
                                        <div>
                                            <CardInfoTitle>Card</CardInfoTitle>
                                            <CardInfo>
                                                <div>
                                                    <strong>Ending in:{' **** **** **** '}</strong>
                                                    {
                                                        (billingInfo.customer
                                                            .default_source as ICard).last4
                                                    }
                                                </div>
                                                <div>
                                                    <strong>Expiry date: </strong>
                                                    {
                                                        (billingInfo.customer
                                                            .default_source as ICard).exp_month
                                                    }
                                                    /
                                                    {
                                                        (billingInfo.customer
                                                            .default_source as ICard).exp_year
                                                    }
                                                </div>
                                                <div>
                                                    <strong>Brand: </strong>
                                                    {
                                                        (billingInfo.customer
                                                            .default_source as ICard).brand
                                                    }
                                                </div>
                                                {accountActive &&
                                                    <div>
                                                        <strong>Next billing date: </strong>
                                                        {this.nextBillingDate()}
                                                    </div>}
                                            </CardInfo>
                                        </div>
                                        <div>
                                            <Button
                                                type="default"
                                                onClick={() => {
                                                    this.setState({ changeCard: true });
                                                }}>
                                                Change card
                                            </Button>
                                            {!accountInfo.isCancelled && (
                                                <Button
                                                    onClick={() => this.showConfirm()}
                                                    type="link">
                                                    Cancel Subscription
                                                </Button>
                                            )}
                                        </div>
                                    </CardInfoWrapper>
                                ) : null}
                            </Col>
                        </Row>
                    ) : (
                            <Loader />
                        )}
                </BillingCard>
                <Drawer
                    onClose={() => this.setState({ changeCard: false })}
                    visible={this.state.changeCard}
                    closable
                    width={400}
                    title="Change card information">
                    <StripeProvider apiKey="pk_test_tt44nyQVK1RTUYVZswEnugMj00Crfda8lI">
                        <Elements>
                            <ChangeBillingFormContainer
                                instagramId={this.props.instagramId}
                                closeDrawer={() => {
                                    this.props.getBillingInfo();
                                    this.setState({ changeCard: false });
                                }}
                            />
                        </Elements>
                    </StripeProvider>
                </Drawer>
                <Drawer
                    onClose={() => this.setState({ changeBilling: false })}
                    visible={this.state.changeBilling}
                    closable
                    width={400}
                    title="Change Billing Information">
                    <Form onSubmit={this.updateCustomer}>
                        <Form.Item label="Name">
                            {getFieldDecorator('name', {
                                rules: [
                                    { required: true, message: 'Please input your name!' },
                                ],
                                initialValue:
                                    billingInfo && billingInfo.customer && billingInfo.customer.name
                                        ? billingInfo.customer.name
                                        : '',
                            })(<Input placeholder="Name" />)}
                        </Form.Item>
                        <Form.Item label="Street address/PO Box">
                            {getFieldDecorator('address.line1', {
                                rules: [{ required: true, message: 'Please input your street address/PO Box!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.line1
                                        : '',
                            })(<Input placeholder="Street address/PO Box" />)}
                        </Form.Item>
                        <Form.Item label="Apartment/Suite/Unit/Building">
                            {getFieldDecorator('address.line2', {
                                rules: [{ required: true, message: 'Please input your address!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.line2
                                        : '',
                            })(<Input placeholder="Apartment/Suite/Unit/Building" />)}
                        </Form.Item>
                        <Form.Item label="City">
                            {getFieldDecorator('address.city', {
                                rules: [{ required: true, message: 'Please input your city!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.city
                                        : '',
                            })(<Input placeholder="City" />)}
                        </Form.Item>
                        <Form.Item label="Zip/Postal Code">
                            {getFieldDecorator('address.postal_code', {
                                rules: [
                                    { required: true, message: 'Please input your Postal Code!' },
                                ],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.postal_code
                                        : '',
                            })(<Input placeholder="Zip/Postal Code" />)}
                        </Form.Item>
                        <Form.Item label="State/Province/County">
                            {getFieldDecorator('address.state', {
                                rules: [{ required: true, message: 'Please input your state!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.state
                                        : '',
                            })(<Input placeholder="State/Province/County" />)}
                        </Form.Item>
                        <Form.Item label="Country">
                            {getFieldDecorator('address.country', {
                                rules: [{ required: true, message: 'Please input your country!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.address
                                        ? billingInfo.customer.address.country
                                        : '',
                            })(
                                <Select
                                    showSearch
                                    placeholder="Country"
                                    optionFilterProp="children"
                                >
                                    {countries.map(country => (
                                        <Select.Option
                                            key={country['alpha-2']}
                                            value={
                                                country['alpha-2']
                                            }
                                        >
                                            {country.name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="VAT">
                            {getFieldDecorator('vat', {
                                rules: [{ required: false, message: 'Please input your vat!' }],
                                initialValue:
                                    billingInfo &&
                                        billingInfo.customer &&
                                        billingInfo.customer.tax_ids &&
                                        billingInfo.customer.tax_ids.data.length > 0
                                        ? billingInfo.customer.tax_ids.data[0].value
                                        : '',
                            })(<Input placeholder="VAT" />)}
                        </Form.Item>
                        {}
                        <Form.Item>
                            <Button loading={this.state.loading} type="primary" htmlType="submit">
                                Update
                            </Button>
                        </Form.Item>
                    </Form>
                </Drawer>
            </BillingPanelStyle>
        );
    }

    private showConfirm() {
        const { instagramId, getBillingInfo, getAccountInfo } = this.props;

        confirm({
            title: 'Do you want to cancel this subscription?',
            content:
                'By cancelling the subscription the website will be removed at the end of the billing period',
            okText: 'No, I made a mistake',
            okType: 'primary',
            cancelText: 'Yes, I want to Cancel',
            maskClosable: true,
            async onCancel() {
                await AccountpageService.cancelSubscription(instagramId);
                getBillingInfo();
                getAccountInfo();

                return;
            },
        });
    }

    private async updateCustomer(e: any) {
        const { instagramId, getBillingInfo } = this.props;

        e.preventDefault();
        this.setState({ loading: true });
        const values = this.props.form.getFieldsValue();

        try {
            await ChargeApiService.updateBillingInfo(instagramId, values.address, values.name, values.vat);
            await getBillingInfo();
            message.success('Your billing information has been updated');
            this.setState({ changeBilling: false, loading: false });
        } catch (error) {
            this.setState({ loading: false });
            message.error('Something went wrong');
        }
    }

    private nextBillingDate() {
        const { billingInfo, accountInfo } = this.props;

        if (billingInfo && billingInfo.subscription) {
            if (
                billingInfo.subscription.cancel_at_period_end &&
                billingInfo.subscription.canceled_at
            ) {
                return `Will be cancelled on ${moment(
                    accountInfo.endDate
                ).format('MMMM Do YYYY')}`;
            } else if (billingInfo.subscription.canceled_at && billingInfo.subscription.cancel_at) {
                return `Was cancelled on ${moment(billingInfo.subscription.cancel_at * 1000).format(
                    ' MMMM Do YYYY'
                )}`;
            } else {
                return `${moment(billingInfo.subscription.current_period_end * 1000).format(
                    'MMMM Do YYYY'
                )}`;
            }
        }

        return 'Unknown';
    }
}
