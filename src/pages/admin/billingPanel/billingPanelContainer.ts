import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import { StoreState } from '@store/reducers/root';

import { BillingPanel } from '@pages/admin/billingPanel/billingPanel';
import { Form } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { AccountInfo } from '@api/accountPage';
import { BillingInfo } from '@api/charge';

export interface BillingPanelContainerProps {
    instagramId: string;
    accountInfo: AccountInfo;
    billingInfo?: BillingInfo;
    accountActive: boolean;
    getAccountInfo(): void;
    getBillingInfo(): void;
}

export const mapStateToProps = (state: StoreState) => {
    return {};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
    return {};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withForm = Form.create();

export const BillingPanelContainer = compose<React.ComponentType<BillingPanelContainerProps>>(
    withRedux,
    withForm
)(BillingPanel);

export type BillingPanelProps = BillingPanelContainerProps &
    StateProps &
    DispatchProps &
    FormComponentProps;
