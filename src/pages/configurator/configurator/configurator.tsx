import { AccountpageService } from '@api/accountPage';
import { CheckoutModal } from '@components/configurator/checkoutModal/checkoutModal';
import { ConfiguratorSidebarContainer } from '@components/configurator/configuratorSidebar/configuratorSidebarContainer';
import { Loader } from '@components/loader/loader';
import { ConfiguratorProps } from '@pages/configurator/configurator/configuratorContainer';
import {
    ConfiguratorStyle,
    Nav,
    TemplateContent,
} from '@pages/configurator/configurator/configuratorStyle';
import { updateHabitat } from '@utils/habitat/HabitatApp';
import { Button, Icon, message } from 'antd';
import { useModal } from 'hooks/useModal';
import queryString from 'query-string';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { StripeProvider, Elements } from 'react-stripe-elements';

export const Configurator: FC<ConfiguratorProps> = ({
    location,
    accounts,
    match,
    clearConfig,
    clearEditorState,
    history,
    setEdit,
    templateConfig,
    inConfigurationProcess,
}) => {
    const [isLoading, setLoading] = useState(false);
    const [__html, setHtml] = useState('');
    const {
        params: { instagramId },
    } = match;
    const userExists = accounts.some(
        account => account.instagramId === instagramId && account.active
    );
    const { template } = queryString.parse(location.search) as {
        template: string;
    };
    const { modalOpen, setModalOpen } = useModal(false);

    // Helpers --
    useEffect(() => {
        async function fetchHtml() {
            setEdit(true);
            setLoading(true);

            try {
                const html = await AccountpageService.renderTemplate(
                    instagramId,
                    template
                );

                setHtml(html);
                setImmediate(() => {
                    updateHabitat();
                });
            } catch (err) {
                console.error(err);
            }

            setLoading(false);
        }

        fetchHtml();

        return function cleanup() {
            clearEditorState();
        };
    }, [setEdit, setLoading, clearEditorState, instagramId, template]);

    const goHome = useCallback(() => {
        clearConfig();
        history.push('/admin/websites');
    }, [clearConfig, history]);

    const goBack = useCallback(() => history.goBack(), [history]);

    const saveChanges = useCallback(async () => {
        setLoading(true);

        try {
            await AccountpageService.updatePage(instagramId, {
                template,
                templateConfig,
            });

            message.success('Your website has been updated');
            clearConfig();
            history.push('/admin/websites');
        } catch (err) {
            console.error(err);
            message.error('Something went wrong');
        }
    }, [clearConfig, history, instagramId, template, templateConfig]);

    // --

    return (
        <ConfiguratorStyle>
            <TemplateContent isLoading={isLoading}>
                {isLoading ? (
                    <Loader />
                ) : (
                        <div id="htmlArea" dangerouslySetInnerHTML={{ __html }} />
                    )}
            </TemplateContent>
            <div className="header">
                <Nav onClick={goBack}>
                    <Icon type="arrow-left" /> Back
                </Nav>
                <div>
                    {inConfigurationProcess && userExists ? (
                        <>
                            <button onClick={goHome}>Cancel</button>

                            <Button onClick={saveChanges} type="primary">
                                Save
                            </Button>
                        </>
                    ) : (
                            <>
                                <Button
                                    onClick={() => setModalOpen(true)}
                                    type="primary"
                                    size="large"
                                    icon="shopping-cart"
                                >
                                    Checkout
                            </Button>
                                <CheckoutModal
                                    templateName={template}
                                    instagramId={instagramId}
                                    templateConfig={templateConfig}
                                    isVisible={modalOpen}
                                    handleClose={() => setModalOpen(false)}
                                />

                            </>
                        )}
                </div>
            </div>

            <ConfiguratorSidebarContainer />
        </ConfiguratorStyle>
    );
};
