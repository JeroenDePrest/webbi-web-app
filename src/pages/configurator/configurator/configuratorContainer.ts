import { Configurator } from '@pages/configurator/configurator/configurator';
import { clearConfig } from '@store/actions/configurator/clearConfig';
import { clearEditorState } from '@store/actions/configurator/clearEditorState';
import { setEdit } from '@store/actions/configurator/setEdit';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose, Dispatch } from 'redux';
import { injectStripe, ReactStripeElements } from 'react-stripe-elements';

export interface ConfiguratorContainerProps { }

export const mapStateToProps = (state: StoreState) => {
	return {
		accounts: state.auth.accounts,
		inConfigurationProcess: state.configurator.inConfigurationProcess,
		templateConfig: state.editor.moduleConfig,
		moduleSelected: state.editor.moduleSelected,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		clearConfig: () => dispatch(clearConfig({})),
		clearEditorState: () => dispatch(clearEditorState({})),
		setEdit: (inConfigurationProcess: boolean) => dispatch(setEdit({ inConfigurationProcess })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const ConfiguratorContainer = compose<React.ComponentType<ConfiguratorContainerProps>>(
	withRedux,
	withRouter
)(Configurator);

export type ConfiguratorProps = ConfiguratorContainerProps &
	RouteComponentProps<{ instagramId: string }> &
	StateProps &
	DispatchProps;
