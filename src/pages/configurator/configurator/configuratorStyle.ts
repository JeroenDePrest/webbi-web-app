import { ConfiguratorSidebarStyle } from '@components/configurator/configuratorSidebar/configuratorSidebarStyle';
// tslint:disable-next-line:import-name
import styled, { css } from 'styled-components';

interface ConfiguratorStyleProps {
	// hasSidebar?: boolean;
}

export const ConfiguratorStyle = styled.div<ConfiguratorStyleProps>`
	display: grid;
	grid-template-rows: 72px calc(100vh - 72px);
	grid-template-areas:
		"header header"
		"main-content side-bar";
	grid-template-columns: auto 350px;

	.header {
		z-index: 11;
		grid-area: header;

		display: flex;
		align-items: center;
		justify-content: space-between;

		border-bottom: 1px solid #dadada;
		background: white;
		padding: 10px;
		button {
			margin-left: 10px;
		}
	}

	${ConfiguratorSidebarStyle} {
		grid-area: side-bar;
	}
`;

interface TemplateContentProps {
	isLoading: boolean;
}

export const TemplateContent = styled.div<TemplateContentProps>`
	z-index: 0;
	position: relative;
	grid-area: main-content;
	overflow-y: auto;
	${props =>
		props.isLoading &&
		css`
			display: flex;
			align-items: center;
			justify-content: center;
		`}
`;
export const GoBackNav = styled.div`
	width: 100%;
`;

export const ConfiguratorHeader = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	height: 300px;

	p {
		max-width: 700px;
		color: rgba(0, 0, 0, 0.55);
		text-align: center;
		line-height: 1.4;
	}

	@media (max-width: 576px) {
		h1 {
			font-size: 24px !important;
		}

		p {
			font-size: 16px !important;
		}
	}

	@media (max-width: 768px) {
		h1 {
			font-size: 28px;
		}

		p {
			font-size: 18px !important;
		}
	}
`;

export const Nav = styled.div`
	padding: 16px;
	color: rgba(0, 0, 0, 0.85);
	display: flex;
	align-items: center;
	i {
		margin-right: 10px;
	}

	&:hover {
		cursor: pointer;
	}
`;

