import { Template, TemplateApiService } from '@api/templates';
import { DemoModal } from '@components/demoModal/demoModal';
import { Loader } from '@components/loader/loader';
import { TemplateCardContainer } from '@components/templateCard/templateCardContainer';
import { updateHabitat } from '@utils/habitat/HabitatApp';
import { Icon } from 'antd';
import { autobind } from 'core-decorators';
import * as React from 'react';
import { Redirect } from 'react-router';
import { AccountpageService } from '@api/accountPage';
import { TemplatePickerProps } from './templatePickerContainer';
import { Nav, TemplateHeader, TemplatePickerStyle, Templates } from './templatePickerStyle';

interface State {
	templates: Template[];
	selectedTemplate?: Template;
	modalVisible: boolean;
	edit?: string;
	modalInnerHtml: string | undefined;
	url: string;
}

@autobind
export class TemplatePicker extends React.Component<TemplatePickerProps, State> {
	public readonly state: State = {
		templates: [],
		modalVisible: false,
		url: '',
		modalInnerHtml: undefined,
	};

	public componentDidMount() {
		this.setState({ selectedTemplate: this.props.selectedTemplate });

		TemplateApiService.getAllTemplates()
			.then(templates => {
				this.setState({ templates });
			})
			.catch(err => {
				console.error(err);
			});
	}

	public render() {
		if (!this.props.selectedAccount) {
			return <Redirect to="/accounts" />;
		}

		return (
			<TemplatePickerStyle>
				<div onClick={() => this.props.history.goBack()}>
					<Nav>
						<Icon type="arrow-left" /> Back
					</Nav>
				</div>
				<TemplateHeader>
					<h1>Pick your template</h1>
					<p>Choose one of our handmade themes</p>
				</TemplateHeader>
				<Templates>
					{this.state.templates.length === 0 ? <Loader /> : this.renderTemplates()}
				</Templates>

				<DemoModal
					handleCancel={this.handleCancel}
					handleOk={this.handleOk}
					selectedTemplate={this.state.selectedTemplate}
					visible={this.state.modalVisible}
					url={this.state.url}
					innerHtml={this.state.modalInnerHtml}
				/>
			</TemplatePickerStyle>
		);
	}

	private renderTemplates() {
		const templates = [...this.state.templates];

		return templates.map((template: Template, index: number) => {
			const selected = this.state.selectedTemplate
				? this.state.selectedTemplate.name === template.name
				: false;

			return (
				<TemplateCardContainer
					selected={selected}
					select={this.selectTemplate}
					history={this.props.history}
					template={template}
					key={index}
					showDemo={this.showModal}
				/>
			);
		});
	}

	private selectTemplate(template: Template) {
		this.props.selectTemplate(template);

		this.goToConfiguration(template.name);
	}

	private goToConfiguration(template: string) {
		const { selectedAccount } = this.props;

		if (selectedAccount) {
			this.props.history.push(
				`/configurator/${selectedAccount.instagramId}?template=${template}`
			);
		}
	}

	private handleOk() {
		this.setState({
			modalVisible: true,
		});

		if (this.state.selectedTemplate) {
			this.goToConfiguration(this.state.selectedTemplate.name);
		}
	}

	private handleCancel() {
		this.setState({
			modalVisible: false,
		});
	}

	private async showModal(templateName: string) {
		const { selectedAccount } = this.props;

		if (!selectedAccount) {
			return;
		}
		const selectedTemplate = this.state.templates.find(
			template => template.name === templateName
		);

		this.setState({ modalVisible: true, selectedTemplate });

		try {
			const modalInnerHtml = await AccountpageService.renderDemoTemplate(
				selectedAccount.instagramId,
				templateName
			);
			this.setState({ modalInnerHtml }, () => {
				updateHabitat();
			});
		} catch (err) {}
	}
}
