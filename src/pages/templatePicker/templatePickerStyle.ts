// tslint:disable-next-line:import-name
import styled from 'styled-components';

export const TemplatePickerStyle = styled.div`
	padding-bottom: 5rem;
`;

export const TemplateHeader = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	height: 200px;

	p {
		max-width: 700px;
		color: rgba(0, 0, 0, 0.55);
		text-align: center;
		line-height: 1.4;
	}

	@media (max-width: 576px) {
		h1 {
			font-size: 24px !important;
		}

		p {
			font-size: 16px !important;
		}
	}

	@media (max-width: 768px) {
		h1 {
			font-size: 28px;
		}

		p {
			font-size: 18px !important;
		}
	}
`;

export const Templates = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
`;

export const Nav = styled.div`
	padding: 16px;
	color: rgba(0, 0, 0, 0.85);
	font-size: 20px;
	i {
		margin-right: 10px;
	}

	&:hover {
		cursor: pointer;
	}
`;
