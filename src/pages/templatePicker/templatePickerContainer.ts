import { Template } from '@api/templates';
import { TemplatePicker } from '@pages/templatePicker/templatePicker';
import { selectTemplate } from '@store/actions/configurator/selectTemplate';
import { StoreState } from '@store/reducers/root';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose, Dispatch } from 'redux';

export interface TemplatePickerContainerProps {}

export const mapStateToProps = (state: StoreState) => {
	return {
		selectedAccount: state.configurator.selectedAccount,
		selectedTemplate: state.configurator.selectedTemplate,
		inConfigurationProcess: state.configurator.inConfigurationProcess,
	};
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
	return {
		selectTemplate: (template: Template) => dispatch(selectTemplate({ template })),
	};
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(
	mapStateToProps,
	mapDispatchToProps
);

export const TemplatePickerContainer = compose<React.ComponentType<TemplatePickerContainerProps>>(
	withRedux,
	withRouter
)(TemplatePicker);

export type TemplatePickerProps = RouteComponentProps &
	TemplatePickerContainerProps &
	RouteComponentProps &
	StateProps &
	DispatchProps;
