import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import { StoreState } from '@store/reducers/root';

import { Logout } from '@pages/logout/logout';
import { logout } from '@store/actions/auth/logout';

export interface LogoutContainerProps {

}

export const mapStateToProps = (state: StoreState) => {
  return {

  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout: () => dispatch(logout({})),
  };
};

export type StateProps = ReturnType<typeof mapStateToProps>;
export type DispatchProps = ReturnType<typeof mapDispatchToProps>;

const withRedux = connect(mapStateToProps, mapDispatchToProps);

export const LogoutContainer = compose<React.ComponentType<LogoutContainerProps>>(
 withRedux
)(Logout);

export type LogoutProps =
  LogoutContainerProps &
  StateProps &
  DispatchProps;
