import { autobind } from 'core-decorators';
import * as React from 'react';

import { LogoutProps } from '@pages/logout/logoutContainer';
import { LogoutStyle } from '@pages/logout/logoutStyle';

interface State {}

@autobind
export class Logout extends React.Component<LogoutProps, State> {
	public componentDidMount() {
		this.props.logout();
		window.location.href = 'https://webbi.co/';
	}

	public render() {
		return <LogoutStyle />;
	}
}
