import '@style/index.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app';
import * as serviceWorker from './serviceWorker';

declare global {
	interface Window {
		grecaptcha: any;
	}
}

ReactDOM.render(<App />, document.getElementById('app_root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
