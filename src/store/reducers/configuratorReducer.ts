import { clearConfig } from '@store/actions/configurator/clearConfig';
import { selectAccount } from '@store/actions/configurator/selectAccount';
import { selectTemplate } from '@store/actions/configurator/selectTemplate';
import { setEdit } from '@store/actions/configurator/setEdit';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { InstagramBusinessAccount } from './authReducer';

const INITIAL_STATE: ConfiguratorState = {
	inConfigurationProcess: false,
};

export interface ConfiguratorState {
	selectedAccount?: InstagramBusinessAccount;
	selectedTemplate?: any;
	inConfigurationProcess: boolean;
}

export const configuratorReducer = reducerWithInitialState<ConfiguratorState>(INITIAL_STATE)
	.case(selectAccount, (state, props) => ({
		...state,
		selectedAccount: props.account,
	}))
	.case(clearConfig, (state, props) => ({
		...INITIAL_STATE,
	}))
	.case(setEdit, (state, props) => {
		return {
			...state,
			inConfigurationProcess: props.inConfigurationProcess,
			...(props.template && { selectedTemplate: props.template }),
		};
	})
	.case(selectTemplate, (state, props) => ({
		...state,
		selectedTemplate: props.template,
	}));
