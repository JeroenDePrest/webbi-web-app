import { authReducer, AuthState } from '@store/reducers/authReducer';
import { combineReducers } from 'redux';
import { configuratorReducer, ConfiguratorState } from './configuratorReducer';
import { editorReducer, EditorState } from './editorReducer';
import { generalReducer, GeneralState } from './generalReducer';

export const rootReducer = combineReducers<StoreState>({
	auth: authReducer,
	configurator: configuratorReducer,
	general: generalReducer,
	editor: editorReducer,
});

export interface StoreState {
	auth: AuthState;
	configurator: ConfiguratorState;
	general: GeneralState;
	editor: EditorState;
}
