import { clearEditorState } from '@store/actions/configurator/clearEditorState';
import { closeModuleEditor } from '@store/actions/configurator/closeModuleEditor';
import { openModuleEditor } from '@store/actions/configurator/openModuleEditor';
import { registerModule } from '@store/actions/configurator/registerModule';
import { setModuleConfiguration } from '@store/actions/configurator/setModuleConfiguration';
import { updateModuleConfiguration } from '@store/actions/configurator/updateModuleConfiguration';
import { JSONSchema6 } from 'json-schema';
import { get } from 'lodash';
import { reducerWithInitialState } from 'typescript-fsa-reducers';

const INITIAL_STATE: EditorState = {
	moduleSelected: false,
	modules: [],
	moduleConfig: {},
	order: [],
};

export interface SelectedModule {
	id: string;
	// tslint:disable-next-line: no-reserved-keywords
	type: string;
	schema: JSONSchema6;
}

export interface EditorState {
	moduleSelected: boolean;
	selectedModule?: SelectedModule;
	modules: SelectedModule[];
	order: string[];
	moduleConfig: {
		// tslint:disable-next-line: no-reserved-keywords
		[type: string]: {
			// tslint:disable-next-line: no-any
			[id: string]: any;
		};
	};
}

export const editorReducer = reducerWithInitialState<EditorState>(INITIAL_STATE)
	.case(registerModule, (state, props) => {
		return {
			...state,
			order: [...state.order, props.id],
		};
	})
	.case(openModuleEditor, (state, props) => {
		return {
			...state,
			moduleSelected: true,
			selectedModule: {
				id: props.id,
				type: props.moduleType,
				schema: props.schema,
			},
		};
	})
	.case(setModuleConfiguration, (state, props) => {
		const newModules: SelectedModule[] = [
			...state.modules.filter(m => m.id !== props.module.id),
			props.module,
		];

		return {
			...state,
			modules: newModules,
			moduleConfig: {
				...state.moduleConfig,
				[props.module.type]: {
					...get(state.moduleConfig, props.module.type, {}),
					[props.module.id]: props.data,
				},
			},
		};
	})
	.case(updateModuleConfiguration, (state, props) => {
		// let moduleConfig: any = props.data;

		// if (props.module.type === "instagram-feed") {
		// 	const moduleConfigByInstagramType = get(state.moduleConfig, props.module.type, {});

		// 	if (Object.keys(moduleConfigByInstagramType).length > 0) {
		// 		const lastAmount = Object.keys(moduleConfigByInstagramType).reduce((sum, moduleId) => {
		// 			const thisModuleConfig = get(state.moduleConfig, `${props.module.type}.${moduleId}`);

		// 			return sum + (moduleConfig ? get(thisModuleConfig, "amount", 0) as number : 0);
		// 		}, 0);

		// 		moduleConfig = {
		// 			...moduleConfig,
		// 			startForm: lastAmount
		// 		};
		// 	}
		// }

		return {
			...state,
			moduleConfig: {
				...state.moduleConfig,
				[props.module.type]: {
					...get(state.moduleConfig, props.module.type, {}),
					[props.id]: props.data,
				},
			},
		};
	})
	.case(closeModuleEditor, (state, props) => {
		return {
			...state,
			moduleSelected: false,
		};
	})
	.case(clearEditorState, (state, props) => {
		return INITIAL_STATE;
	});
