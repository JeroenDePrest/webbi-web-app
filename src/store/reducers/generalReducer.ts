import { addWebsites } from '@store/actions/general/addWebsites';
import { reducerWithInitialState } from 'typescript-fsa-reducers';

const INITIAL_STATE: GeneralState = {
	websites: [],
};

export interface GeneralState {
	websites: string[];
}

export const generalReducer = reducerWithInitialState<GeneralState>(INITIAL_STATE).case(
	addWebsites,
	(state, props) => ({
		...state,
		websites: props.websites,
	})
);
