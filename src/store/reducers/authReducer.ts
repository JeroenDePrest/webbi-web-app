import { login } from '@store/actions/auth/login';
import { logout } from '@store/actions/auth/logout';
import { AuthService } from '@utils/auth/authService';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { store } from '@utils/habitat/HabitatApp';
import { AuthApiService, User } from '@api/auth';
import { setUserData } from '@store/actions/auth/setUserData';

const INITIAL_STATE: AuthState = {
	isAuthenticated: false,
	accounts: [],
};

export interface AuthState {
	isAuthenticated: boolean;
	user?: any;
	profile?: User['profile'];
	accounts: InstagramBusinessAccount[];
}

export interface InstagramBusinessAccount {
	instagramId: string;
	username: string;
	profilePictureUrl?: string;
	pageName: string;
	active: boolean;
}

const decodedToken = AuthService.getDecodedToken();

if (decodedToken) {
	INITIAL_STATE.isAuthenticated = true;
	INITIAL_STATE.user = decodedToken;

	AuthApiService.me()
		.then(userData => {
			store.dispatch(setUserData(userData));
		})
		.catch(error => {
			if (error.response && error.response.status === 401) {
				AuthService.logout();
			}
		});
}

export const authReducer = reducerWithInitialState<AuthState>(INITIAL_STATE)
	.case(setUserData, (state, props) => {
		return {
			...state,
			profile: props.profile,
			accounts: props.accounts,
		};
	})
	.case(login, (state, props) => {
		const decoded = props.user ? props.user : AuthService.getDecodedToken(props.jwt);

		if (props.jwt) {
			AuthService.storeJwt(props.jwt);
		}

		return {
			...state,
			isAuthenticated: true,
			// tslint:disable-next-line: no-unsafe-any
			user: decoded,
		};
	})
	.case(logout, (state, props) => {
		AuthService.logout();

		return {
			...state,
			isAuthenticated: false,
		};
	});
