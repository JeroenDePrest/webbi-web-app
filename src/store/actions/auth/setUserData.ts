import actionCreatorFactory from 'typescript-fsa';
import { User } from '@api/auth';
import { InstagramBusinessAccount } from '@store/reducers/authReducer';

const actionCreator = actionCreatorFactory('@@AUTH/');

export interface SetUserDataActionProperties {
	profile: User['profile'];
	accounts: InstagramBusinessAccount[];
}

export const setUserData = actionCreator<SetUserDataActionProperties>('SETUSERDATA');
