import { JSONSchema6 } from 'json-schema';
import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface OpenModuleEditorActionProperties {
	id: string;
	moduleType: string;
	schema: JSONSchema6;
}

export const openModuleEditor = actionCreator<OpenModuleEditorActionProperties>('OPENMODULEEDITOR');
