import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface RegisterModuleActionProperties {
	id: string;
}

export const registerModule = actionCreator<RegisterModuleActionProperties>('REGISTERMODULE');
