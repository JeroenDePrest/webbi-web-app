import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface ClearConfigActionProperties {}

export const clearConfig = actionCreator<ClearConfigActionProperties>('CLEARCONFIG');
