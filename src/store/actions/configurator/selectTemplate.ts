import actionCreatorFactory from 'typescript-fsa';
import { Template } from '@api/templates';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface SelectTemplateActionProperties {
    template: Template;
}

export const selectTemplate = actionCreator<SelectTemplateActionProperties>('SELECTTEMPLATE');