import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface SetEditActionProperties {
	inConfigurationProcess: boolean;
	template?: string;
}

export const setEdit = actionCreator<SetEditActionProperties>('SETEDIT');
