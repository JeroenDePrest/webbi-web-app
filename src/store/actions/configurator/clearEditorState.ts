import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface ClearEditorStateActionProperties {}

export const clearEditorState = actionCreator<ClearEditorStateActionProperties>('CLEAREDITORSTATE');
