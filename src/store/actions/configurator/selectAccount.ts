import { InstagramBusinessAccount } from '@store/reducers/authReducer';
import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@ACCOUNTS/');

export interface SelectAccountActionProperties {
	account: InstagramBusinessAccount;
}

export const selectAccount = actionCreator<SelectAccountActionProperties>('SELECTACCOUNT');
