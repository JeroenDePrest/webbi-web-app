import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface CloseModuleEditorActionProperties {}

export const closeModuleEditor = actionCreator<CloseModuleEditorActionProperties>(
	'CLOSEMODULEEDITOR'
);
