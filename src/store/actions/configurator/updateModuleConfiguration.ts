import { SelectedModule } from '@store/reducers/editorReducer';
import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface UpdateModuleConfigurationActionProperties {
	id: string;
	data: object;
	module: SelectedModule;
}

export const updateModuleConfiguration = actionCreator<UpdateModuleConfigurationActionProperties>(
	'UPDATEMODULECONFIGURATION'
);
