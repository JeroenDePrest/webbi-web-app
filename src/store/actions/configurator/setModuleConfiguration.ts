import { SelectedModule } from '@store/reducers/editorReducer';
import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@CONFIGURATOR/');

export interface AddModuleConfigurationActionProperties {
	data: object;
	module: SelectedModule;
}

export const setModuleConfiguration = actionCreator<AddModuleConfigurationActionProperties>(
	'SETMODULECONFIGURATION'
);
