import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('@@GENERAL/');

export interface AddWebsitesActionProperties {
	websites: string[];
}

export const addWebsites = actionCreator<AddWebsitesActionProperties>('ADDWEBSITES');
