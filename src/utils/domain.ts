export const getSubdomain = () => {
	const reg = /(?:(.*?)\.)[^.]*\.[^.]*$/;
	const { 1: subdomain } = window.location.hostname.match(reg) || [];

	return subdomain;
};
