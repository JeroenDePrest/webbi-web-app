export const FB_AUTH_URL = process.env.REACT_APP_FB_AUTH_URL;
export const FB_CLIENT_ID = process.env.REACT_APP_FB_CLIENT_ID;
export const FB_SCOPE = process.env.REACT_APP_FB_SCOPE;
export const API_URL = process.env.REACT_APP_API_URL || '/api';
export const RECAPTCHA_SITE_KEY = process.env.REACT_APP_RECAPTCHA_SITE_KEY;
