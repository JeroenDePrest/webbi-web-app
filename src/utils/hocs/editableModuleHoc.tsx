import { TemplateContext } from '@api/templates';
import { ConditionalWrapper } from '@components/common/conditionalWrapper/conditionalWrapper';
import { ModuleToolbeltVisibleState } from '@components/configurator/moduleToolbeltVisibleState/moduleToolbeltVisibleState';
import { openModuleEditor } from '@store/actions/configurator/openModuleEditor';
import { setModuleConfiguration } from '@store/actions/configurator/setModuleConfiguration';
import { SelectedModule } from '@store/reducers/editorReducer';
import { StoreState } from '@store/reducers/root';
import { getConfigForModule } from '@utils/getConfigForModule';
import { autobind } from 'core-decorators';
import { JSONSchema6 } from 'json-schema';
import defaults from 'json-schema-defaults';
import { get } from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

export interface InjectedEditableModuleConfig {
	// tslint:disable-next-line: no-reserved-keywords
	type: string;
	schema: JSONSchema6;
}

export interface InjectedEditableModuleProps<C> extends InjectedEditableModuleConfig {
	id: string;
	templateContext: TemplateContext;
	moduleConfig: C;
}

// tslint:disable-next-line: max-func-body-length
export const makeEditableModule = <P extends object>(config: InjectedEditableModuleConfig) => {
	const mapStateToProps = (
		state: StoreState,
		props: { id: string; templateContext: TemplateContext }
	) => {
		const { selectedTemplate } = state.configurator;

		return {
			inConfigurationProcess: state.configurator.inConfigurationProcess,
			selectedTemplate: selectedTemplate ? selectedTemplate.name : undefined,
			moduleSelected: state.editor.moduleSelected,
			selectedModule: state.editor.selectedModule,
			moduleConfig: get(
				state.editor.moduleConfig,
				`${config.type}.${props.id}`,
				defaults(config.schema)
			),
		};
	};

	const mapDispatchToProps = (dispatch: Dispatch) => {
		return {
			openModuleEditor: (id: string, moduleType: string, schema: JSONSchema6) =>
				dispatch(openModuleEditor({ id, moduleType, schema })),
			setModuleConfiguration: (selectedModule: SelectedModule, data: object) =>
				dispatch(setModuleConfiguration({ data, module: selectedModule })),
		};
	};

	type StateProps = ReturnType<typeof mapStateToProps>;
	type DispatchProps = ReturnType<typeof mapDispatchToProps>;

	interface EditableModuleProps extends StateProps, DispatchProps {
		id: string;
		templateContext: TemplateContext;
	}

	const withEditableModule = <P extends EditableModuleProps>() => (
		Component: React.ComponentType<P & EditableModuleProps>
	) => {
		@autobind
		class MakeEditableModule extends React.Component<
		P & EditableModuleProps
		> {
			public componentDidMount() {
				const { templateContext, id, selectedTemplate } = this.props;

				const moduleConfig =
					templateContext.moduleConfigs &&
						Object.keys(templateContext.moduleConfigs).length &&
						(selectedTemplate === templateContext.originalTemplate || !selectedTemplate)
						? getConfigForModule(id, config.type, templateContext.moduleConfigs)
						: this.props.moduleConfig;

				this.props.setModuleConfiguration(
					{
						...config,
						id: this.props.id,
					},
					moduleConfig
				);
			}

			public render() {
				const { id, moduleSelected, selectedModule, inConfigurationProcess } = this.props;
				const openInEditor =
					moduleSelected &&
					selectedModule &&
					selectedModule.type === config.type &&
					id === selectedModule.id;

				return (
					<ConditionalWrapper
						condition={inConfigurationProcess}
						wrapper={children => (
							<ModuleToolbeltVisibleState
								openInEditor={openInEditor}
								openModuleEditor={() => {
									this.props.openModuleEditor(
										this.props.id,
										config.type,
										config.schema
									);
								}}>
								{children}
							</ModuleToolbeltVisibleState>
						)}>
						<Component {...(this.props as any)} {...config} />
					</ConditionalWrapper>
				);
			}
		}

		return MakeEditableModule;
	};

	return compose(
		connect(
			mapStateToProps,
			mapDispatchToProps
		),
		withEditableModule()
	);
};
