import { get } from 'lodash';

// tslint:disable-next-line: no-reserved-keywords
export const getConfigForModule = (id: string, type: string, moduleConfig: object) => {
	return get(moduleConfig, `${type}.${id}`, {});
};
