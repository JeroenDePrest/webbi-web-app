import { configureStore } from '@store/configureStore';
import ReactHabitat from 'react-habitat';
import { RECAPTCHA_SITE_KEY } from '@utils/env';
import { ReduxDomFactory } from './ReduxDomFactory';

const { store } = configureStore({});

export { store };

export class ReactHabitatApp extends ReactHabitat.Bootstrapper {
	constructor() {
		super();

		// Create a new container
		const containerBuilder = new ReactHabitat.ContainerBuilder();

		// Set the container to use our redux factory
		containerBuilder.factory = new ReduxDomFactory(store);

		// Register our components that we want to expose to the DOM
		containerBuilder
			.registerAsync(() =>
				import('../../components/modules/instagramFeedContainer').then(
					({ InstagramFeedContainer }) => InstagramFeedContainer
				)
			)
			.as('InstagramFeed');
		containerBuilder
			.registerAsync(() =>
				import('../../components/modules/textBlockContainer').then(
					({ TextBlockContainer }) => TextBlockContainer
				)
			)
			.as('TextBlock');
		containerBuilder
			.registerAsync(() =>
				import('../../components/modules/contactFormContainer').then(
					({ ContactFormContainer }) => ContactFormContainer
				)
			)
			.withDefaultProps({
				recaptchaSiteKey: RECAPTCHA_SITE_KEY,
			})
			.as('ContactForm');
		// containerBuilder
		// 	.registerAsync(() => System.import("./containers/AddAlbumForm"))
		// 	.as("RAddAlbumForm");

		// Set the DOM container
		this.setContainer(containerBuilder.build());
	}
}

export const HabitatApp = new ReactHabitatApp();

export const updateHabitat = HabitatApp.update.bind(
	HabitatApp,
	document.getElementById('htmlArea') || undefined
);
