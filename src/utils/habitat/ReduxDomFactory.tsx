import { registerModule } from '@store/actions/configurator/registerModule';
import { StoreState } from '@store/reducers/root';
import { ComponentClass, default as React, FunctionComponent } from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'redux';

/**
 * React Redux DOM Factory
 */
export class ReduxDomFactory {
	private readonly store: Store<StoreState>;

	constructor(store: Store<StoreState>) {
		this.store = store;
	}

	public inject(
		moduleName: FunctionComponent | string | ComponentClass,
		props: any = {},
		target: Element
	) {
		const componentName = props.proxy.dataset.component;
		const moduleId = props.id;

		this.store.dispatch(registerModule({ id: moduleId }));

		if (target) {
			ReactDom.render(
				<Provider store={this.store}>{React.createElement(moduleName, props)}</Provider>,
				target
			);
		} else {
			console.warn('Target element is null or undefined. Cannot inject component');
		}
	}

	public dispose(target: Element) {
		if (target) {
			ReactDom.unmountComponentAtNode(target);
		}
	}
}
